from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base
from sqlalchemy import func
from openpatch_itembank.models.item_version import ItemVersion
import uuid


class TestEdge(Base):
    __tablename__ = gt("test_edge")
    __mapper_args__ = {"confirm_deleted_rows": False}

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)

    node_from_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("test_node"))))
    node_to_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("test_node"))))
    threshold = db.Column(db.Integer)

    node_from = db.relationship(
        "TestNode", back_populates="edges", foreign_keys=[node_from_id]
    )
    node_to = db.relationship("TestNode", foreign_keys=[node_to_id])

    def copy(self, uuid_map):
        test_edge = TestEdge(
            threshold=self.threshold,
            node_from_id=uuid_map[self.node_from_id],
            node_to_id=uuid_map[self.node_to_id],
        )

        return test_edge


class TestItem(Base):
    __tablename__ = gt("test_item")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    node_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("test_node"))))
    version = db.Column(db.Integer)
    item_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("item"))))
    index = db.Column(db.Integer)

    node = db.relationship("TestNode", back_populates="items")
    item = db.relationship("Item", viewonly=True)

    __table_args__ = (
        db.ForeignKeyConstraint(
            ["version", "item_id"],
            [
                "{}.version".format(gt("item_version")),
                "{}.item_id".format(gt("item_version")),
            ],
        ),
    )
    item_version = db.relationship(
        "ItemVersion",
        primaryjoin="and_(TestItem.version==ItemVersion.version, TestItem.item_id==ItemVersion.item_id)",
    )

    def copy(self):
        test_item = TestItem(item_version=self.item_version, index=self.index)

        return test_item


class TestNode(Base):
    __tablename__ = gt("test_node")
    __mapper_args__ = {"confirm_deleted_rows": False}

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    test_version_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("test_version")))
    )
    name = db.Column(db.String(255))
    index = db.Column(db.Integer)

    x = db.Column(db.Integer)
    y = db.Column(db.Integer)

    # node flow
    flow = db.Column(db.String(64), default="linear")

    # settings
    encrypted = db.Column(db.Boolean())
    needs_to_be_correct = db.Column(db.Boolean, default=False)
    randomized = db.Column(db.Boolean, default=False)

    # termination criterions
    time_limit = db.Column(db.Integer, default=0)
    item_limit = db.Column(db.Integer, default=0)
    score_limit = db.Column(db.Integer, default=0)
    precision_limit = db.Column(db.Integer, default=0)

    # node type
    start = db.Column(db.Boolean, default=False)
    end = db.Column(db.Boolean, default=False)

    items = db.relationship("TestItem", back_populates="node")
    edges = db.relationship(
        "TestEdge",
        foreign_keys=lambda: TestEdge.node_from_id,
        back_populates="node_from",
        cascade="all, delete, delete-orphan",
    )
    test_version = db.relationship("TestVersion", back_populates="nodes")

    @classmethod
    def clean(cls):
        floating_nodes = cls.query.filter(cls.test_version_id == None).all()

        for node in floating_nodes:
            floating_edges = TestEdge.query.filter(
                db.or_(TestEdge.node_to == node, TestEdge.node_from == node)
            ).all()
            for edge in floating_edges:
                db.session.delete(edge)

        for node in floating_nodes:
            db.session.delete(node)

    def copy(self, uuid_map):
        test_node = TestNode(
            id=uuid_map[self.id],
            name=self.name,
            index=self.index,
            flow=self.flow,
            needs_to_be_correct=self.needs_to_be_correct,
            randomized=self.randomized,
            time_limit=self.time_limit,
            item_limit=self.item_limit,
            score_limit=self.score_limit,
            precision_limit=self.precision_limit,
            start=self.start,
            end=self.end,
        )

        for item in self.items:
            test_node.items.append(item.copy())

        for edge in self.edges:
            test_node.edges.append(edge.copy(uuid_map))

        return test_node


class TestVersion(Base):
    __tablename__ = gt("test_version")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    version = db.Column(db.Integer)
    version_message = db.Column(db.Text)
    status = db.Column(db.String(20))
    latest = db.Column(db.Boolean)

    test_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("test"))))
    test = db.relationship("Test", back_populates="versions")
    nodes = db.relationship("TestNode", back_populates="test_version")
    member = db.relationship("Member", back_populates="test_versions")

    def copy(self):
        latest_version = (
            db.session.query(func.max(TestVersion.version))
            .filter(TestVersion.test == self.test)
            .scalar()
        )

        test_version = TestVersion(
            member=self.member,
            version=latest_version + 1,
            version_message=self.version_message,
            status=self.status,
            test=self.test,
            latest=self.latest,
        )

        uuid_map = {}
        for node in self.nodes:
            uuid_map[node.id] = uuid.uuid4()

        for node in self.nodes:
            test_version.nodes.append(node.copy(uuid_map))

        return test_version

    @classmethod
    def get_draft(cls, test_id):
        return cls.query.filter_by(test_id=test_id, status="draft").first()

    def get_item_versions(self):
        item_versions = (
            db.session.query(ItemVersion)
            .join(TestItem)
            .join(TestNode)
            .join(TestVersion)
            .filter(TestVersion.id == self.id)
            .all()
        )
        return item_versions

    @classmethod
    def get_latest(cls, test_id):
        return cls.query.filter_by(test_id=test_id, latest=True).first()

    def permitted_write(self, jwt_claims):
        return self.test.permitted_write(jwt_claims)

    def permitted_read(self, jwt_claims):
        return self.test.permitted_read(jwt_claims)
