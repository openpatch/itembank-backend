from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID

association_item_category = db.Table(
    gt("item_category"),
    Base.metadata,
    db.Column("item_id", GUID(), db.ForeignKey("{}.id".format(gt("item")))),
    db.Column(
        "category_id", db.String(255), db.ForeignKey("{}.id".format(gt("category")))
    ),
)
