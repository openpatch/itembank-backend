from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID

association_item_tag = db.Table(
    gt("item_tag"),
    Base.metadata,
    db.Column("item_id", GUID(), db.ForeignKey("{}.id".format(gt("item")))),
    db.Column("tag_id", db.String(255), db.ForeignKey("{}.id".format(gt("tag")))),
)
