from enum import Enum


class Privacy(Enum):
    public = 1
    private = 2
    notlisted = 3
