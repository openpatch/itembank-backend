from openpatch_core.database import db, gt
from openpatch_core.database.types import GUID
from openpatch_core.models import Base


class Member(Base):
    __tablename__ = gt("member")

    id = db.Column(GUID(), primary_key=True)
    username = db.Column(db.String(64))
    avatar_id = db.Column(GUID())
    full_name = db.Column(db.String(128))

    collections = db.relationship("Collection", back_populates="member")

    items = db.relationship("Item", back_populates="member")
    item_versions = db.relationship("ItemVersion", back_populates="member")

    tests = db.relationship("Test", back_populates="member")
    test_versions = db.relationship("TestVersion", back_populates="member")

    @classmethod
    def get_or_create(cls, jwt):
        member = cls.query.get(jwt.get("id"))

        if not member:
            member = cls(id=jwt.get("id"))

        member.username = jwt.get("username")
        member.avatar_id = jwt.get("avatar_id")
        member.full_name = jwt.get("full_name")

        db.session.add(member)
        db.session.commit()
        return member
