from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID


class TestCollection(Base):
    __tablename__ = gt("test_collection")

    test_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("test"))), primary_key=True
    )
    collection_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("collection"))), primary_key=True
    )
    position = db.Column(db.Integer())

    test = db.relationship("Test", back_populates="collections")
    collection = db.relationship("Collection", back_populates="tests")

    @classmethod
    def get_or_create(cls, test_id, collection_id):
        test_collection = cls.query.get(
            {"test_id": test_id, "collection_id": collection_id}
        )
        if not test_collection:
            test_collection = cls(test_id=test_id, collection_id=collection_id)

        return test_collection
