from sqlalchemy import event
from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from openpatch_itembank.utils import draft
from openpatch_itembank.models.test_remix import association_test_remix
from openpatch_itembank.models.privacy import Privacy
import uuid


class Test(Base):
    __tablename__ = gt("test")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    name = db.Column(db.String(255), nullable=False)
    privacy = db.Column(db.Enum(Privacy), default=Privacy.private)
    public_description = db.Column(db.JSON)
    public_description_text = db.Column(db.Text)

    member = db.relationship("Member", back_populates="tests")
    versions = db.relationship(
        "TestVersion", back_populates="test", order_by="TestVersion.version"
    )
    collections = db.relationship("TestCollection", back_populates="test")
    remixes = db.relationship(
        "Test",
        secondary=association_test_remix,
        primaryjoin=id == association_test_remix.c.test_id,
        secondaryjoin=id == association_test_remix.c.remix_id,
    )

    def permitted_read(self, jwt_claims):
        return self.privacy != Privacy.private or (
            jwt_claims
            and (
                str(self.member_id) == jwt_claims["id"] or jwt_claims["role"] == "admin"
            )
        )

    def permitted_write(self, jwt_claims):
        return str(self.member_id) == jwt_claims["id"] or jwt_claims["role"] == "admin"


@event.listens_for(Test.public_description, "set")
def public_description_text_set(target, value, oldvalue, initiator):
    target.public_description_text = draft.to_text(value)
