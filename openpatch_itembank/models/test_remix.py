from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID

association_test_remix = db.Table(
    gt("test_remix"),
    Base.metadata,
    db.Column("test_id", GUID(), db.ForeignKey("{}.id".format(gt("test")))),
    db.Column("remix_id", GUID(), db.ForeignKey("{}.id".format(gt("test")))),
)
