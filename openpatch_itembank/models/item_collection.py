from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID


class ItemCollection(Base):
    __tablename__ = gt("item_collection")

    item_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("item"))), primary_key=True
    )
    collection_id = db.Column(
        GUID(), db.ForeignKey("{}.id".format(gt("collection"))), primary_key=True
    )
    position = db.Column(db.Integer())

    item = db.relationship("Item", back_populates="collections")
    collection = db.relationship("Collection", back_populates="items")

    @classmethod
    def get_or_create(cls, item_id, collection_id):
        item_collection = cls.query.get(
            {"item_id": item_id, "collection_id": collection_id}
        )
        if not item_collection:
            item_collection = cls(item_id=item_id, collection_id=collection_id)

        return item_collection
