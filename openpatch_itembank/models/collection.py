from sqlalchemy import event
from sqlalchemy.ext.orderinglist import ordering_list
from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from openpatch_itembank.utils import draft
from openpatch_itembank.models.privacy import Privacy
import uuid


class Collection(Base):
    __tablename__ = gt("collection")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    name = db.Column(db.String(255), nullable=False)
    public_description = db.Column(db.JSON)
    public_description_text = db.Column(db.Text)
    privacy = db.Column(db.Enum(Privacy), default=Privacy.private)

    items = db.relationship(
        "ItemCollection",
        order_by="ItemCollection.position",
        collection_class=ordering_list("position"),
        cascade="all, delete-orphan",
    )
    tests = db.relationship(
        "TestCollection",
        order_by="TestCollection.position",
        collection_class=ordering_list("position"),
        cascade="all, delete-orphan",
    )

    member = db.relationship("Member", back_populates="collections")

    def permitted_write(self, jwt_claims):
        return jwt_claims["id"] == str(self.member_id) or jwt_claims["role"] == "admin"

    def permitted_read(self, jwt_claims):
        return self.privacy != Privacy.private or (
            jwt_claims
            and (
                jwt_claims["id"] == str(self.member_id) or jwt_claims["role"] == "admin"
            )
        )


@event.listens_for(Collection.public_description, "set")
def public_description_text_set(target, value, oldvalue, initiator):
    target.public_description_text = draft.to_text(value)
