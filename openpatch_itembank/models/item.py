from sqlalchemy import event, select, func
from sqlalchemy.orm import column_property
from openpatch_itembank.models.item_version import ItemVersion
from openpatch_itembank.models.test_version import TestItem
from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from openpatch_itembank.utils import draft
from openpatch_itembank.models.item_tag import association_item_tag
from openpatch_itembank.models.item_category import association_item_category
from openpatch_itembank.models.item_remix import association_item_remix
from openpatch_itembank.models.privacy import Privacy
import uuid


class Item(Base):
    __tablename__ = gt("item")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    name = db.Column(db.String(255), nullable=False)
    public_description = db.Column(db.JSON)
    public_description_text = db.Column(db.Text)
    source_reference = db.Column(db.Text)
    privacy = db.Column(db.Enum(Privacy), default=Privacy.private)
    language = db.Column(db.String(5))
    used_in_tests = column_property(
        select([func.count(TestItem.id)])
        .where(TestItem.item_id == id)
        .correlate_except(TestItem)
    )

    tags = db.relationship(
        "Tag", secondary=association_item_tag, back_populates="items"
    )
    categories = db.relationship(
        "Category", secondary=association_item_category, back_populates="items"
    )
    collections = db.relationship("ItemCollection", back_populates="item")
    remixes = db.relationship(
        "Item",
        secondary=association_item_remix,
        primaryjoin=id == association_item_remix.c.item_id,
        secondaryjoin=id == association_item_remix.c.remix_id,
    )

    versions = db.relationship(
        "ItemVersion", back_populates="item", order_by="ItemVersion.version"
    )
    member = db.relationship("Member", back_populates="items")

    @classmethod
    def query_latest(cls):
        return cls.query.join(ItemVersion, cls.id == ItemVersion.item_id).filter(
            ItemVersion.latest == 1
        )

    def permitted_write(self, jwt_claims):
        return jwt_claims["id"] == str(self.member_id) or jwt_claims["role"] == "admin"

    def permitted_read(self, jwt_claims):
        return self.privacy != Privacy.private or (
            jwt_claims
            and (
                jwt_claims["id"] == str(self.member_id)
                or jwt_claims["id"]
                or jwt_claims["role"] == "admin"
            )
        )


@event.listens_for(Item.public_description, "set")
def public_description_text_set(target, value, oldvalue, initiator):
    target.public_description_text = draft.to_text(value)
