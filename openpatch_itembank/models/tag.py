from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from openpatch_itembank.models.item_tag import association_item_tag
import uuid


class Tag(Base):
    __tablename__ = gt("tag")

    id = db.Column(db.String(255), primary_key=True)

    items = db.relationship(
        "Item", secondary=association_item_tag, back_populates="tags"
    )

    def permitted_read(self, jwt_claims):
        return True

    def permitted_write(self, jwt_claims):
        return jwt_claims["role"] == "admin"
