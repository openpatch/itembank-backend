from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from openpatch_itembank.models.item_category import association_item_category
import uuid


class Category(Base):
    __tablename__ = gt("category")

    id = db.Column(db.String(255), primary_key=True)
    color = db.Column(db.String(7))

    items = db.relationship(
        "Item", secondary=association_item_category, back_populates="categories"
    )

    def permitted_read(self, jwt_claims):
        return True

    def permitted_write(self, jwt_claims):
        return jwt_claims["role"] == "admin"
