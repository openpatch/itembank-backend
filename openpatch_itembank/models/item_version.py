from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID
from sqlalchemy import func
from sqlalchemy.ext.orderinglist import ordering_list
import uuid


class ItemTask(Base):
    __tablename__ = gt("item_task")

    id = db.Column(GUID(), primary_key=True, default=uuid.uuid4)
    version = db.Column(db.Integer)
    item_id = db.Column(GUID())
    task = db.Column(db.Text)
    text = db.Column(db.JSON)
    format_type = db.Column(db.String(60))
    format_version = db.Column(db.Integer)
    data = db.Column(db.JSON)
    evaluation = db.Column(db.JSON)
    position = db.Column(db.Integer)

    __table_args__ = (
        db.ForeignKeyConstraint(
            ["version", "item_id"],
            [
                "{}.version".format(gt("item_version")),
                "{}.item_id".format(gt("item_version")),
            ],
        ),
    )
    item_version = db.relationship(
        "ItemVersion",
        primaryjoin="and_(ItemTask.version==ItemVersion.version, ItemTask.item_id==ItemVersion.item_id)",
        viewonly=True,
    )

    def copy(self):
        item_task = ItemTask(
            task=self.task,
            text=self.text,
            format_type=self.format_type,
            format_version=self.format_version,
            data=self.data,
            evaluation=self.evaluation,
        )
        return item_task


class ItemVersion(Base):
    __tablename__ = gt("item_version")

    member_id = db.Column(GUID(), db.ForeignKey("{}.id".format(gt("member"))))
    version = db.Column(db.Integer, primary_key=True)
    version_message = db.Column(db.Text)
    status = db.Column(db.String(20))
    latest = db.Column(db.Boolean)

    item_id = db.Column(
        GUID, db.ForeignKey("{}.id".format(gt("item"))), primary_key=True
    )
    item = db.relationship("Item", back_populates="versions")

    tasks = db.relationship(
        "ItemTask",
        order_by="ItemTask.position",
        collection_class=ordering_list("position"),
    )
    member = db.relationship("Member", back_populates="item_versions")

    @classmethod
    def get_draft(cls, item_id):
        return cls.query.filter_by(item_id=item_id, status="draft").first()

    def copy(self):
        latest_version = (
            db.session.query(func.max(ItemVersion.version))
            .filter(ItemVersion.item == self.item)
            .scalar()
        )
        item_version = ItemVersion(
            member=self.member,
            version=latest_version + 1,
            version_message=self.version_message,
            status=self.status,
            item=self.item,
            latest=self.latest,
        )

        for task in self.tasks:
            item_version.tasks.append(task.copy())

        return item_version

    @classmethod
    def get_latest(cls, item_id):
        return cls.query.filter_by(item_id=item_id, latest=True).first()

    def permitted_write(self, jwt_claims):
        return self.item.permitted_write(jwt_claims)

    def permitted_read(self, jwt_claims):
        return self.item.permitted_read(jwt_claims)
