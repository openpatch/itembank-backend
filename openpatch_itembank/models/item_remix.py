from openpatch_core.database import db, gt
from openpatch_core.models import Base
from openpatch_core.database.types import GUID

association_item_remix = db.Table(
    gt("item_remix"),
    Base.metadata,
    db.Column("item_id", GUID(), db.ForeignKey("{}.id".format(gt("item")))),
    db.Column("remix_id", GUID(), db.ForeignKey("{}.id".format(gt("item")))),
)
