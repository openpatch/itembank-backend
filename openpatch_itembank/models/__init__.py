# isort:skip_file
from openpatch_core.database import db
from openpatch_itembank.models import (
    category,
    item_collection,
    item,
    item_version,
    member,
    tag,
    test_collection,
    test,
    test_version,
    collection,
)
from sqlalchemy import orm

orm.configure_mappers()
