def to_text(draft):
    if not draft:
        return draft

    text = ""
    if isinstance(draft, str):
        return text.strip()

    for block in draft.get("blocks", []):
        text += block.get("text", "") + " "
    return text.strip()
