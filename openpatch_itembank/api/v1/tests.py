import uuid
from sqlalchemy.exc import DataError, OperationalError, IntegrityError, StatementError
from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_itembank.api.v1 import api, errors
from openpatch_core.database import db
from openpatch_core.rabbitmq import rabbit
from openpatch_core.jwt import get_jwt_claims, jwt_required
from openpatch_itembank.models.member import Member
from openpatch_itembank.models.test import Test
from openpatch_itembank.models.test_version import TestVersion, TestEdge, TestNode
from openpatch_itembank.models.privacy import Privacy
from openpatch_itembank.api.v1.schemas.test import TESTS_SCHEMA, TEST_SCHEMA
from openpatch_itembank.api.v1.schemas.test_version import (
    TEST_VERSION_SCHEMA,
    TEST_VERSIONS_SCHEMA,
    TEST_EDGES_SCHEMA,
    TEST_ITEMS_SCHEMA,
)
from openpatch_itembank.api.v1.schemas.member import MEMBER_SCHEMA
from openpatch_itembank.api.v1.schemas.item_version import ITEM_VERSIONS_SCHEMA

BASE_URL = "/tests"


@api.route(BASE_URL, methods=["POST", "GET"])
def tests():
    if request.method == "POST":
        return post_tests()
    return get_tests()


@jwt_required
def post_tests():
    """
    @api {post} /v1/tests Post tests
    @apiVersion 1.0.0
    @apiName PostTests
    @apiGroup Tests

    @apiUse jwt

    @apiParam {Number} id The test id.
    @apiParam {String{1..255}} name Name
    @apiParam {String} private_description Private hidden description
    @apiParam {String} public_description Public description
    @apiParam {Boolean} public Is the item visible to all user

    @apiSuccess {Number} test_id Test id

    @apiUse errors_no_json
    @apiUse errors_invalid_json

    @apiPermission admin
    @apiPermission author
    """
    jwt_claims = get_jwt_claims()

    test_json = request.get_json()
    if not test_json:
        return errors.no_json()

    member = Member.get_or_create(jwt_claims)

    if "member" in test_json:
        del test_json["member"]

    try:
        test = TEST_SCHEMA.load(test_json, session=db.session)
        test.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(test)
    db.session.commit()

    test_version = TestVersion(
        latest=True,
        status="draft",
        test=test,
        version=1,
        version_message="Initial",
        member=member,
    )

    db.session.add(test_version)
    db.session.commit()

    if test.privacy == Privacy.public:
        rabbit.publish_activity(
            "create",
            {"id": test.id, "type": "test", "data": TEST_SCHEMA.dump(test)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({"test_id": test.id})


def get_tests():
    """
    @api {get} /v1/tests Get tests
    @apiVersion 1.0.0
    @apiName GetTests
    @apiGroup Tests

    @apiUse jwt
    @apiUse elastic_query

    @apiSuccess {Object[]} tests
    @apiSuccess {Number} tests.id The test id.
    @apiSuccess {String{1..255}} tests.name Name
    @apiSuccess {String} tests.private_description Private hidden description
    @apiSuccess {String} tests.public_description Public description
    @apiSuccess {Boolean} tests.public Is the item visible to all user
    """
    jwt_claims = get_jwt_claims(optional=True)

    test_query, count, page = Test.elastic_query(request.args.get("query", "{}"))

    if not jwt_claims:
        test_query = test_query.filter(Test.privacy == Privacy.public)
    elif jwt_claims.get("role") != "admin":
        test_query = test_query.filter(
            db.or_(
                Test.privacy == Privacy.public, Test.member_id == jwt_claims.get("id")
            )
        )

    test_query = page(query=test_query)
    tests = test_query.all()

    return jsonify({"tests": TESTS_SCHEMA.dump(tests), "tests_count": count})


@api.route(BASE_URL + "/<test_id>", methods=["PUT", "GET", "DELETE"])
def test(test_id):
    if request.method == "PUT":
        return put_test(test_id)
    elif request.method == "DELETE":
        return delete_test(test_id)
    return get_test(test_id)


@jwt_required
def put_test(test_id):
    """
    @api {put} /v1/tests/:id Put test
    @apiVersion 1.0.0
    @apiName PutTest
    @apiGroup Tests

    @apiUse jwt

    @apiParam {String{1..255}} name Name
    @apiParam {String} private_description Private hidden description
    @apiParam {String} public_description Public description
    @apiParam {Boolean} public Is the item visible to all user

    @apiSuccess {Number} test_id Test id

    @apiUse errors_no_json
    @apiUse errors_invalid_json
    @apiUse errors_resource_not_found
    @apiUse errors_access_not_allowed

    @apiPermission admin
    @apiPermission author
    """
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    test_json = request.get_json()
    if not test_json:
        return errors.no_json()

    if "member" in test_json:
        del test_json["member"]

    if "versions" in test_json:
        del test_json["versions"]

    try:
        test = Test.query.get(test_id)
    except StatementError:
        return errors.resource_not_found()

    if not test:
        return errors.resource_not_found()

    if not test.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    try:
        result = TEST_SCHEMA.load(
            test_json, session=db.session, instance=test, partial=True
        )
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.commit()

    if test.privacy == Privacy.public:
        rabbit.publish_activity(
            "update",
            {"id": test.id, "type": "test", "data": TEST_SCHEMA.dump(test)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({"test_id": test.id}), 200


@jwt_required
def delete_test(test_id):
    """
    @api {delete} /v1/tests/:id Delete test
    @apiVersion 1.0.0
    @apiName DeleteTest
    @apiGroup Tests

    @apiUse jwt

    @apiParam {Number} id Test id

    @apiUse errors_resource_not_found
    @apiUse errors_access_not_allowed

    @apiPermission admin
    @apiPermission author
    """
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    try:
        test = Test.query.get(test_id)
    except StatementError:
        return errors.resource_not_found()

    if not test:
        return errors.resource_not_found()

    if not test.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    nodes = []
    for version in test.versions:
        for test_node in version.nodes:
            for test_item in test_node.items:
                db.session.delete(test_item)
            for test_edge in test_node.edges:
                db.session.delete(test_edge)
            nodes.append(test_node)
        for node in nodes:
            db.session.delete(node)
        db.session.delete(version)

    db.session.delete(test)

    if test.privacy == Privacy.public:
        rabbit.publish_activity(
            "delete",
            {"id": test.id, "type": "test", "data": TEST_SCHEMA.dump(test)},
            MEMBER_SCHEMA.dump(member),
        )

    db.session.commit()

    return jsonify({}), 200


@api.route(BASE_URL + "/<test_id>/remix", methods=["POST"])
@jwt_required
def remix_test(test_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    try:
        test = Test.query.get(test_id)
    except StatementError:
        return errors.resource_not_found()

    if not test:
        return errors.resource_not_found()

    if not test.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    new_test = Test(
        name=test.name + " (Remix)",
        public_description=test.public_description,
        public_description_text=test.public_description_text,
        privacy=test.privacy,
        member=member,
    )

    db.session.add(new_test)

    latest_test_version = TestVersion.get_latest(test_id)
    if not latest_test_version:
        latest_test_version = TestVersion.get_draft(test_id)
    if not latest_test_version:
        return errors.resource_not_found()
    with db.session.no_autoflush:
        new_test_version = latest_test_version.copy()
        new_test_version.member = member
        new_test_version.version = 1
        new_test_version.version_message = "remix"
        new_test_version.status = "draft"
        new_test_version.latest = True
        new_test_version.test = new_test

        test.remixes.append(new_test)

        db.session.add(new_test_version)
        db.session.commit()

    if new_test.privacy == Privacy.public:
        rabbit.publish_activity(
            "create:remix",
            {"id": test.id, "type": "test", "data": TEST_SCHEMA.dump(test)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({"test_id": new_test.id})


def get_test(test_id):
    """
    @api {get} /v1/tests/:id Get test
    @apiVersion 1.0.0
    @apiName GetTest
    @apiGroup Tests

    @apiUse jwt

    @apiParam {Number} id Test id

    @apiSuccess {Object} test
    @apiSuccess {Number} test.id The test id.
    @apiSuccess {String{1..255}} test.name Name
    @apiSuccess {String} test.private_description Private hidden description
    @apiSuccess {String} test.public_description Public description
    @apiSuccess {Boolean} test.public Is the item visible to all user

    @apiUse errors_resource_not_found
    @apiUse errors_access_not_allowed
    """
    jwt_claims = get_jwt_claims(optional=True)

    try:
        test = Test.query.get(test_id)
    except StatementError:
        return errors.resource_not_found()

    if not test:
        return errors.resource_not_found()

    if not test.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    return jsonify({"test": TEST_SCHEMA.dump(test)})


@api.route(BASE_URL + "/<test_id>/versions/<version>", methods=["GET"])
def get_test_version(test_id, version):
    jwt_claims = get_jwt_claims(optional=True)

    test_version_query = TestVersion.query.filter_by(test_id=test_id)
    if version == "latest":
        test_version_query = test_version_query.filter_by(latest=True)
    elif version == "draft":
        test_version_query = test_version_query.filter_by(status="draft")
    else:
        test_version_query = test_version_query.filter_by(version=version)

    try:
        test_version = test_version_query.first()
    except StatementError:
        return errors.resource_not_found()

    if not test_version:
        return errors.resource_not_found()

    if not test_version.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    return jsonify({"test_version": TEST_VERSION_SCHEMA.dump(test_version)})


@api.route(BASE_URL + "/<test_id>/versions", methods=["GET", "POST", "PUT"])
def test_versions(test_id):
    if request.method == "POST":
        return post_test_versions(test_id)
    elif request.method == "PUT":
        return put_test_versions(test_id)
    return get_test_versions(test_id)


def get_test_versions(test_id):
    """
    @api {get} /v1/tests/:id/versions Get test versions
    @apiVersion 1.0.0
    @apiName GetTestVersions
    @apiGroup Tests

    @apiUse jwt
    @apiUse elastic_query
    """
    jwt_claims = get_jwt_claims(optional=True)

    try:
        test = Test.query.get(test_id)
    except StatementError:
        return errors.resource_not_found()

    if not test:
        return errors.resource_not_found()

    if not test.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    test_versions_query, count, page = TestVersion.elastic_query(
        request.args.get("query", "{}")
    )
    test_versions_query = test_versions_query.filter_by(test=test)

    if jwt_claims.get("role") != "admin":
        test_versions_query = test_versions_query.filter(
            db.or_(
                Test.privacy != Privacy.private, Test.member_id == jwt_claims.get("id")
            )
        )

    test_versions_query = page(query=test_versions_query)
    test_versions = test_versions_query.all()

    return jsonify(
        {
            "test_versions": TEST_VERSIONS_SCHEMA.dump(test_versions),
            "test_versions_count": count,
        }
    )


@jwt_required
def put_test_versions(test_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    test_version_json = request.get_json()

    if not test_version_json:
        return errors.no_json()

    test_version_json["status"] = "draft"
    if "id" in test_version_json:
        del test_version_json["id"]
    if "version" in test_version_json:
        del test_version_json["version"]
    if "latest" in test_version_json:
        del test_version_json["latest"]
    if "member" in test_version_json:
        del test_version_json["member"]

    test_version = TestVersion.get_draft(test_id)
    if not test_version:
        return errors.resource_not_found()

    try:
        test = Test.query.get(test_id)
    except StatementError:
        return errors.resource_not_found()

    if not test.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    test_version_edges_json = []

    node_ids = []
    for node in test_version_json.get("nodes", []):
        node_ids.append(node["id"])

    for node in test_version_json.get("nodes", []):
        for edge in node.get("edges", []):
            if edge["node_from"] in node_ids and edge["node_to"] in node_ids:
                test_version_edges_json.append(edge)
        node["edges"] = []

    try:
        with db.session.no_autoflush:
            result = TEST_VERSION_SCHEMA.load(
                test_version_json,
                session=db.session,
                instance=test_version,
                partial=("id", "version", "version_message", "latest"),
            )
            result.member = member

    except ValidationError as e:
        db.session.rollback()
        return errors.invalid_json(e.messages)

    try:
        db.session.commit()

        edges = TEST_EDGES_SCHEMA.load(test_version_edges_json, session=db.session)
        db.session.add_all(edges)

        db.session.commit()
    except (OperationalError, IntegrityError, StatementError) as e:
        db.session.rollback()
        return errors.invalid_json("Malformed request")

    TestNode.clean()

    if test.privacy == Privacy.public:
        rabbit.publish_activity(
            "update:version",
            {"id": test.id, "type": "test", "data": TEST_SCHEMA.dump(test)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({}), 200


@jwt_required
def post_test_versions(test_id):
    # aka new draft
    jwt_claims = get_jwt_claims()

    test_version_json = request.get_json()
    if not test_version_json:
        return errors.no_json()

    member = Member.get_or_create(jwt_claims)

    draft_test_version = TestVersion.get_draft(test_id)
    latest_test_version = TestVersion.get_latest(test_id)
    if not draft_test_version:
        return errors.resource_not_found()

    with db.session.no_autoflush:
        new_draft_test_version = draft_test_version.copy()
        draft_test_version.status = "pilot"
        draft_test_version.latest = True
        draft_test_version.version_message = test_version_json["version_message"]
        new_draft_test_version.version_message = "draft"
        new_draft_test_version.version = draft_test_version.version + 1
        new_draft_test_version.member = member

        if latest_test_version:
            latest_test_version.latest = False
        db.session.add(new_draft_test_version)
        db.session.commit()

    if test.privacy == Privacy.public:
        rabbit.publish_activity(
            "create:version",
            {"id": test.id, "type": "test", "data": TEST_SCHEMA.dump(test)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify(
        {
            "test_version_id": new_draft_test_version.id,
            "version": new_draft_test_version.version,
        }
    )


@api.route(BASE_URL + "/<test_id>/versions/<version>/status", methods=["PUT"])
@jwt_required
def put_test_version_status(test_id, version):
    json = request.get_json()
    if not json:
        return errors.no_json()

    status = json.get("status")

    if not status or status not in ["pilot", "ready", "faulty"]:
        return errors.invalid_json("Wrong status")

    jwt_claims = get_jwt_claims()

    test_version = TestVersion.query.filter_by(test_id=test_id, version=version).first()

    if not test_version:
        return errors.resource_not_found()

    if not test_version.permitted_write(jwt_claims) or test_version.status == "draft":
        return errors.access_not_allowed()

    try:
        test_version.status = status
        db.session.commit()
    except DataError as e:
        db.session.rollback()
        return errors.invalid_json(e.messages)

    return jsonify({}), 200


@api.route(BASE_URL + "/<test_id>/versions/<version>/item-versions", methods=["GET"])
def get_test_version_item_versions(test_id, version):
    jwt_claims = get_jwt_claims(optional=True)

    test_version = TestVersion.query.filter_by(test_id=test_id, version=version).first()

    if not test_version:
        return errors.resource_not_found()

    if not test_version.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    item_versions = test_version.get_item_versions()

    return jsonify({"item_versions": ITEM_VERSIONS_SCHEMA.dump(item_versions)})
