from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_itembank.api.v1 import api, errors
from openpatch_itembank.api.v1.schemas.category import (
    CATEGORY_SCHEMA,
    CATEGORIES_SCHEMA,
)
from openpatch_itembank.api.v1.schemas.item import ITEMS_SCHEMA
from openpatch_itembank.models.category import Category
from openpatch_core.database import db
from openpatch_core.jwt import get_jwt_claims, jwt_required, jwt_roles_required

BASE_URL = "/categories"


@api.route(BASE_URL, methods=["POST", "GET"])
def categories():
    if request.method == "POST":
        return post_categories()
    return get_categories()


@jwt_roles_required(["admin"])
def post_categories():
    claims = get_jwt_claims()

    category_json = request.get_json()

    if not category_json:
        return errors.no_json()

    try:
        result = CATEGORY_SCHEMA.load(category_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    category = result

    db.session.add(category)
    db.session.commit()

    return jsonify({"category_id": category.id}), 200


def get_categories():
    claims = get_jwt_claims(optional=True)

    categories_query, count, page = Category.elastic_query(
        request.args.get("query", "{}")
    )
    categories_query = page(query=categories_query)
    categories = categories_query.all()

    return jsonify({"categories": CATEGORIES_SCHEMA.dump(categories)}), 200


@api.route(BASE_URL + "/<category_id>", methods=["DELETE", "GET", "PUT"])
def category(category_id):
    if request.method == "DELETE":
        return delete_category(category_id)
    elif request.method == "PUT":
        return put_category(category_id)
    return get_category(category_id)


@jwt_roles_required(["admin"])
def delete_category(category_id):
    claims = get_jwt_claims()

    category = Category.query.get(category_id)

    if not category:
        return errors.resource_not_found()

    if not category.permitted_write(claims):
        return errors.access_not_allowed()

    db.session.delete(category)
    db.session.commit()

    return jsonify({}), 200


def get_category(category_id):
    claims = get_jwt_claims(optional=True)

    category = Category.query.get(category_id)

    if not category:
        return errors.resource_not_found()

    if not category.permitted_read(claims):
        return errors.access_not_allowed()

    return (
        jsonify(
            {
                "category": CATEGORY_SCHEMA.dump(category),
                "items": ITEMS_SCHEMA.dump(category.items),
            }
        ),
        200,
    )


@jwt_roles_required(["admin"])
def put_category(category_id):
    claims = get_jwt_claims()

    category_json = request.get_json()
    if not category_json:
        return errors.no_json()

    category = Category.query.get(category_id)

    if not category:
        return errors.resource_not_found()

    if not category.permitted_write(claims):
        return errors.access_not_allowed()

    try:
        result = CATEGORY_SCHEMA.load(
            category_json, session=db.session, instance=category, partial=True
        )
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(result)
    db.session.commit()

    return jsonify({"status": "success"}), 200
