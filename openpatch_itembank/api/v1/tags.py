from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_itembank.api.v1 import api, errors
from openpatch_itembank.api.v1.schemas.tag import TAG_SCHEMA, TAGS_SCHEMA
from openpatch_itembank.models.tag import Tag
from openpatch_core.database import db
from openpatch_core.jwt import get_jwt_claims, jwt_required, jwt_roles_required

BASE_URL = "/tags"


@api.route(BASE_URL, methods=["POST", "GET"])
def tags():
    if request.method == "POST":
        return post_tags()
    return get_tags()


@jwt_required
def post_tags():
    claims = get_jwt_claims()

    tag_json = request.get_json()

    if not tag_json:
        return errors.no_json()

    try:
        result = TAG_SCHEMA.load(tag_json, session=db.session)
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    tag = result

    db.session.add(tag)
    db.session.commit()

    return jsonify({"tag_id": tag.id}), 200


def get_tags():
    tags_query, count, page = Tag.elastic_query(request.args.get("query", "{}"))

    tags_query = page(query=tags_query)
    tags = tags_query.all()

    return jsonify({"tags": TAGS_SCHEMA.dump(tags)}), 200


@api.route(BASE_URL + "/<tag_id>", methods=["DELETE", "GET", "PUT"])
def tag(tag_id):
    if request.method == "DELETE":
        return delete_tag(tag_id)
    elif request.method == "PUT":
        return put_tag(tag_id)
    return get_tag(tag_id)


@jwt_roles_required(["admin"])
def delete_tag(tag_id):
    claims = get_jwt_claims()

    tag = Tag.query.get(tag_id)

    if not tag:
        return errors.resource_not_found()

    if not tag.permitted_write(claims):
        return errors.access_not_allowed()

    db.session.delete(tag)
    db.session.commit()

    return jsonify({}), 200


def get_tag(tag_id):
    claims = get_jwt_claims(optional=True)

    tag = Tag.query.get(tag_id)

    if not tag:
        return errors.resource_not_found()

    if not tag.permitted_read(claims):
        return errors.access_not_allowed()

    return jsonify({"tag": TAG_SCHEMA.dump(tag)}), 200


@jwt_roles_required(["admin"])
def put_tag(tag_id):
    claims = get_jwt_claims()

    tag_json = request.get_json()
    if not tag_json:
        return errors.no_json()

    tag = Tag.query.get(tag_id)

    if not tag:
        return errors.resource_not_found()

    if not tag.permitted_write(claims):
        return errors.access_not_allowed()

    try:
        result = TAG_SCHEMA.load(
            tag_json, session=db.session, instance=tag, partial=True
        )
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(result)
    db.session.commit()

    return jsonify({"status": "success"}), 200
