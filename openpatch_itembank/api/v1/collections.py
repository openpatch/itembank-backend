from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_itembank.api.v1 import api, errors
from openpatch_itembank.api.v1.schemas.collection import (
    COLLECTION_SCHEMA,
    COLLECTIONS_SCHEMA,
)
from openpatch_itembank.api.v1.schemas.item import ITEMS_SCHEMA
from openpatch_itembank.api.v1.schemas.test import TESTS_SCHEMA
from openpatch_itembank.models.member import Member
from openpatch_itembank.models.item import Item
from openpatch_itembank.models.test import Test
from openpatch_itembank.models.privacy import Privacy
from openpatch_itembank.models.collection import Collection
from openpatch_itembank.models.item_collection import ItemCollection
from openpatch_itembank.models.test_collection import TestCollection
from openpatch_core.database import db
from openpatch_core.jwt import get_jwt_claims, jwt_required

BASE_URL = "/collections"


@api.route(BASE_URL, methods=["POST", "GET"])
def collections():
    if request.method == "POST":
        return post_collections()
    return get_collections()


@jwt_required
def post_collections():
    claims = get_jwt_claims()

    collection_json = request.get_json()

    if not collection_json:
        return errors.no_json()

    member = Member.get_or_create(claims)

    if "member" in collection_json:
        del collection_json["member"]

    try:
        result = COLLECTION_SCHEMA.load(collection_json, session=db.session)
        result.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    collection = result

    db.session.add(collection)
    db.session.commit()

    return jsonify({"collection_id": collection.id}), 200


def get_collections():
    claims = get_jwt_claims(optional=True)

    collections_query, count, page = Collection.elastic_query(
        request.args.get("query", "{}")
    )

    if not claims:
        collections_query = collections_query.filter(
            Collection.privacy == Privacy.public
        )
    elif claims.get("role") != "admin":
        collections_query = collections_query.filter(
            db.or_(
                Collection.member_id == claims.get("id"),
                Collection.privacy == Privacy.public,
            )
        )

    collections_query = page(query=collections_query)
    collections = collections_query.all()

    return jsonify({"collections": COLLECTIONS_SCHEMA.dump(collections)}), 200


@api.route(BASE_URL + "/<collection_id>", methods=["DELETE", "GET", "PUT"])
def collection(collection_id):
    if request.method == "DELETE":
        return delete_collection(collection_id)
    elif request.method == "PUT":
        return put_collection(collection_id)
    return get_collection(collection_id)


@jwt_required
def delete_collection(collection_id):
    claims = get_jwt_claims()

    collection = Collection.query.get(collection_id)

    if not collection:
        return errors.resource_not_found()

    if not collection.permitted_write(claims):
        return errors.access_not_allowed()

    db.session.delete(collection)
    db.session.commit()

    return jsonify({}), 200


def get_collection(collection_id):
    claims = get_jwt_claims(optional=True)

    collection = Collection.query.get(collection_id)

    if not collection:
        return errors.resource_not_found()

    if not collection.permitted_read(claims):
        return errors.access_not_allowed()

    # Get item and test out of the collections
    items = [item.item for item in collection.items]
    tests = [test.test for test in collection.tests]

    return (
        jsonify(
            {
                "collection": COLLECTION_SCHEMA.dump(collection),
                "items": ITEMS_SCHEMA.dump(items),
                "tests": TESTS_SCHEMA.dump(tests),
            }
        ),
        200,
    )


@jwt_required
def put_collection(collection_id):
    claims = get_jwt_claims()

    collection_json = request.get_json()
    if not collection_json:
        return errors.no_json()

    member = Member.get_or_create(claims)

    if "member" in collection_json:
        del collection_json["member"]

    items = collection_json.get("items")
    tests = collection_json.get("tests")

    if items:
        del collection_json["items"]

    if tests:
        del collection_json["tests"]

    collection = Collection.query.get(collection_id)

    if not collection:
        return errors.resource_not_found()

    if not collection.permitted_write(claims):
        return errors.access_not_allowed()

    try:
        result = COLLECTION_SCHEMA.load(
            collection_json, session=db.session, instance=collection, partial=True
        )
        result.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    if items:
        item_collections = []
        position = 0
        for item in items:
            item_collection = ItemCollection.get_or_create(item, collection.id)
            item_collection.position = position
            item_collections.append(item_collection)
            position += 1
        result.items = item_collections

    if tests:
        test_collections = []
        position = 0
        for test in tests:
            test_collection = TestCollection.get_or_create(test, collection.id)
            test_collection.position = position
            test_collections.append(test_collection)
            position += 1
        result.tests = test_collections

    db.session.add(result)
    db.session.commit()

    return jsonify({"status": "success"}), 200


@api.route(BASE_URL + "/<collection_id>/items/<item_id>", methods=["DELETE", "POST"])
@jwt_required
def collection_item(collection_id, item_id):
    claims = get_jwt_claims()
    member = Member.get_or_create(claims)
    collection = Collection.query.get(collection_id)
    item = Item.query.get(item_id)

    if not collection or not item:
        return errors.resource_not_found()

    if not collection.permitted_write(claims) or not item.permitted_read(claims):
        return errors.access_not_allowed()

    item_collection = ItemCollection.query.get(
        ({"collection_id": collection_id, "item_id": item_id})
    )
    if request.method == "DELETE":
        db.session.delete(item_collection)
    elif request.method == "POST" and not item_collection:
        item_collection = ItemCollection(collection_id=collection_id, item_id=item_id)
        collection.items.append(item_collection)

    db.session.commit()

    return jsonify({"status": "success"}), 200


@api.route(BASE_URL + "/<collection_id>/tests/<test_id>", methods=["DELETE", "POST"])
@jwt_required
def collection_test(collection_id, test_id):
    claims = get_jwt_claims()
    member = Member.get_or_create(claims)
    collection = Collection.query.get(collection_id)
    test = Test.query.get(test_id)

    if not collection or not test:
        return errors.resource_not_found()

    if not collection.permitted_write(claims) or not test.permitted_read(claims):
        return errors.access_not_allowed()

    test_collection = TestCollection.query.get(
        ({"collection_id": collection_id, "test_id": test_id})
    )
    if request.method == "DELETE":
        db.session.delete(test_collection)
    elif request.method == "POST" and not test_collection:
        test_collection = TestCollection(collection_id=collection_id, test_id=test_id)
        collection.tests.append(test_collection)

    db.session.commit()

    return jsonify({"status": "success"}), 200
