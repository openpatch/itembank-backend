# isort:skip_file
import os
from flask import Blueprint

from flask_cors import CORS

api = Blueprint("api_v1", __name__, template_folder="templates", static_folder="static")

CORS(api, origins=[os.getenv("OPENPATCH_ORIGINS", "*")])

from openpatch_itembank.api.v1 import items
from openpatch_itembank.api.v1 import tags
from openpatch_itembank.api.v1 import tests
from openpatch_itembank.api.v1 import categories
from openpatch_itembank.api.v1 import collections
