import os
import requests
import json
from marshmallow import validates_schema, ValidationError, fields, EXCLUDE, pre_load
from openpatch_core.schemas import ma
from openpatch_core.database import db
from openpatch_itembank.models.item_version import ItemVersion, ItemTask
from openpatch_itembank.api.v1.schemas.member import MemberSchema

format_url = os.getenv("OPENPATCH_FORMAT_SERVICE")

if not format_url:
    raise Exception("OPENPATCH_FORMAT_SERVICE environment variable not set")

format_validate_url = "%s/v1/validate" % format_url


class ItemTaskSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemTask
        sqla_session = db.session
        unknown = EXCLUDE
        load_instance = True

    id = fields.UUID()
    format_version = fields.Int(default=1)

    @pre_load(pass_many=True)
    def reposition_tasks(self, data, **kwargs):
        position = 0
        for task in data:
            task["position"] = position
            position += 1
        return data

    @pre_load
    def validate_format(self, data, **kwargs):
        errors = {}
        try:
            r = requests.post(
                format_validate_url,
                json={
                    "data": data.get("data", {}),
                    "format_type": data.get("format_type"),
                    "evaluation": data.get("evaluation", {"skip": True}),
                },
            )
            if r.status_code >= 500:
                errors["task"] = "Format service unavailable"
            elif r.status_code >= 400:
                errors["task"] = r.json().get("message")
            elif not r.json().get("correct"):
                errors["task"] = r.json().get("details")
            task_data = r.json().get("task", {}).get("data")
            evluation_data = r.json().get("task", {}).get("evaluation")
            if task_data:
                data["data"] = task_data
            if evluation_data:
                data["evaluation"] = evluation_data
            return data

        except requests.ConnectionError:
            errors["task"] = "Format service unavailable"

        if errors:
            raise ValidationError(errors)


ITEM_TASK_SCHEMA = ItemTaskSchema()
ITEM_TASKS_SCHEMA = ItemTaskSchema(many=True)


class ItemVersionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemVersion
        sqla_session = db.session
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    member = fields.Nested(
        MemberSchema, allow_none=True, only=["id", "username", "avatar_id", "full_name"]
    )

    tasks = fields.Nested(ItemTaskSchema, many=True)


ITEM_VERSION_SCHEMA = ItemVersionSchema()
ITEM_VERSIONS_SCHEMA = ItemVersionSchema(many=True)
