from marshmallow import fields
from openpatch_itembank.models.tag import Tag
from openpatch_core.schemas import ma
from openpatch_itembank.api.v1.schemas.member import MemberSchema


class TagSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        include_fk = True
        model = Tag
        load_instance = True


TAG_SCHEMA = TagSchema()
TAGS_SCHEMA = TagSchema(many=True)
