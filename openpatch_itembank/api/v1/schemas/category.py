from marshmallow import fields
from openpatch_core.schemas import ma, EnumField
from openpatch_itembank.models.category import Category
from openpatch_itembank.api.v1.schemas.member import MemberSchema


class CategorySchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        include_fk = True
        model = Category
        load_instance = True


CATEGORY_SCHEMA = CategorySchema()
CATEGORIES_SCHEMA = CategorySchema(many=True)
