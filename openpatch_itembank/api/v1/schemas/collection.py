from marshmallow import fields, EXCLUDE
from openpatch_core.schemas import ma, EnumField
from openpatch_core.database import db
from openpatch_itembank.api.v1.schemas.member import MemberSchema
from openpatch_itembank.models.collection import Collection
from openpatch_itembank.models.privacy import Privacy
from openpatch_itembank.models.item_collection import ItemCollection
from openpatch_itembank.models.test_collection import TestCollection


class ItemCollectionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = ItemCollection
        sqla_session = db.session
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True


class TestCollectionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = TestCollection
        sqla_session = db.session
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True


class CollectionSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Collection
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True

    privacy = EnumField(Privacy)
    member = fields.Nested(
        MemberSchema, allow_none=True, only=["id", "username", "avatar_id", "full_name"]
    )
    items = fields.Pluck(ItemCollectionSchema, "item", many=True)
    tests = fields.Pluck(TestCollectionSchema, "test", many=True)


COLLECTION_SCHEMA = CollectionSchema()
COLLECTIONS_SCHEMA = CollectionSchema(many=True)
