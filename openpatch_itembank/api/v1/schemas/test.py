from marshmallow import fields, EXCLUDE
from openpatch_core.schemas import ma, EnumField
from openpatch_itembank.api.v1.schemas.test_version import TestVersionSchema
from openpatch_itembank.api.v1.schemas.member import MemberSchema
from openpatch_itembank.api.v1.schemas.member import MemberSchema
from openpatch_itembank.api.v1.schemas.collection import TestCollectionSchema
from openpatch_itembank.models.privacy import Privacy
from openpatch_itembank.models.test import Test


class TestSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Test
        exclude = ["public_description_text"]
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    privacy = EnumField(Privacy)
    member = fields.Nested(
        MemberSchema, allow_none=True, only=["id", "username", "avatar_id", "full_name"]
    )

    versions = fields.List(
        fields.Nested(
            TestVersionSchema(
                only=("test", "version", "version_message", "status", "latest")
            )
        )
    )

    collections = fields.Pluck(
        TestCollectionSchema, "collection", many=True, dump_only=True
    )


TEST_SCHEMA = TestSchema()
TESTS_SCHEMA = TestSchema(many=True)
