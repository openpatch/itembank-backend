from marshmallow import fields, EXCLUDE
from openpatch_itembank.models.member import Member
from openpatch_core.schemas import ma
from openpatch_core.database import db


class MemberSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Member
        sqla_session = db.session
        exclude = ["tests", "test_versions", "items", "item_versions"]
        unknown = EXCLUDE
        load_instance = True

    id = fields.UUID()


MEMBER_SCHEMA = MemberSchema()
MEMBERS_SCHEMA = MemberSchema(many=True)
