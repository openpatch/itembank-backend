from marshmallow import fields, pre_load, pre_dump, EXCLUDE
from openpatch_itembank.models.item import Item
from openpatch_itembank.api.v1.schemas.item_version import ItemVersionSchema
from openpatch_itembank.api.v1.schemas.member import MemberSchema
from openpatch_itembank.api.v1.schemas.tag import TagSchema
from openpatch_itembank.api.v1.schemas.collection import ItemCollectionSchema
from openpatch_itembank.models.tag import Tag
from openpatch_itembank.models.privacy import Privacy
from openpatch_core.schemas import ma, EnumField


class ItemSchema(ma.SQLAlchemyAutoSchema):
    class Meta:
        model = Item
        exclude = ["public_description_text"]
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    privacy = EnumField(Privacy)
    member = fields.Nested(
        MemberSchema, allow_none=True, only=["id", "username", "avatar_id", "full_name"]
    )
    versions = fields.List(
        fields.Nested(
            ItemVersionSchema(
                only=("item", "version", "version_message", "status", "latest")
            )
        )
    )
    used_in_tests = fields.Int(dump_only=True)
    collections = fields.Pluck(
        ItemCollectionSchema, "collection", many=True, dump_only=True
    )


ITEM_SCHEMA = ItemSchema()
ITEMS_SCHEMA = ItemSchema(many=True)
