import os
from marshmallow import (
    pre_load,
    post_dump,
    validates,
    ValidationError,
    EXCLUDE,
    fields,
    validates_schema,
    validate,
)
import json
from marshmallow_sqlalchemy import SQLAlchemyAutoSchema
from openpatch_core.schemas import ma
from openpatch_core.database import db
from openpatch_itembank.models.test_version import (
    TestVersion,
    TestNode,
    TestEdge,
    TestItem,
)
from openpatch_itembank.api.v1.schemas.item_version import ItemVersionSchema
from openpatch_itembank.api.v1.schemas.item import ItemSchema
from openpatch_itembank.api.v1.schemas.member import MemberSchema


class TestEdgeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TestEdge
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    threshold = fields.Int(validate=validate.Range(min=0), required=True)


class TestItemSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TestItem
        sqla_session = db.session
        unknown = EXCLUDE
        load_instance = True
        include_relationships = True

    id = fields.UUID()


class TestNodeSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TestNode
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    name = fields.String(required=True, allow_none=False)

    flow = fields.String(
        allow_none=True, validate=validate.OneOf(["adaptive", "linear", "jumpable"])
    )

    item_limit = fields.Int(validate=validate.Range(min=0))
    time_limit = fields.Int(validate=validate.Range(min=0))
    score_limit = fields.Int(validate=validate.Range(min=0))
    precision_limit = fields.Int(validate=validate.Range(min=0))

    items = fields.List(ma.Nested(TestItemSchema(exclude=("item",))))
    edges = fields.List(ma.Nested(TestEdgeSchema))

    @validates_schema
    def validate_node_settings(self, data, **kwargs):
        flow = data.get("flow", "linear")
        score_limit = data.get("score_limit", 0)
        time_limit = data.get("time_limit", 0)
        item_limit = data.get("item_limit", 0)
        precision_limit = data.get("precision_limit", 0)
        randomized = data.get("randomized", False)
        needs_to_be_correct = data.get("needs_to_be_correct", False)

        # if needs_to_be_correct and time_limit > 0:
        #    raise ValidationError(
        #        "a node can not have a time limit and needs to be correct. This could leed to a deadlock."
        #    )

        # if needs_to_be_correct and score_limit > 0:
        #    raise ValidationError(
        #        "a node can not have a score limit and needs to be correct."
        #    )

        # if score_limit > len(data.get("items", [])):
        #    raise ValidationError("the score limit is higher than the number of items.")

        # if item_limit > len(data.get("items", [])):
        #    raise ValidationError("the item limit is higher than the number of items.")

        # if flow == "adaptive":
        #    if randomized:
        #        raise ValidationError("a adaptive node can not be randomized.")
        #    if needs_to_be_correct:
        #        raise ValidationError(
        #            "a adpative node can not be forced to be correct."
        #        )
        #    if (
        #        item_limit == 0
        #        and time_limit == 0
        #        and score_limit == 0
        #        and precision_limit == 0
        #    ):
        #        raise ValidationError(
        #            "a termination criterion needs to be set for adaptive nodes."
        #        )

    @validates_schema
    def validate_no_edges_when_end(self, data, **kwargs):
        if data.get("end", False) and len(data.get("edges", [])) > 0:
            raise ValidationError("end node is not allowed to have edges")

    @validates_schema(pass_original=True)
    def validate_has_default_edge(self, data, original_data, **kwargs):
        has_default_edge = False
        for edge in data.get("edges", []):
            if edge.threshold == 0:
                has_default_edge = True
                return

        if not has_default_edge and len(data.get("edges", [])) > 0:
            raise ValidationError("node needs a default edge with threshold 0", "edges")


class TestVersionSchema(SQLAlchemyAutoSchema):
    class Meta:
        model = TestVersion
        sqla_session = db.session
        load_instance = True
        include_relationships = True

    id = fields.UUID()
    member = fields.Nested(
        MemberSchema, allow_none=True, only=["id", "username", "avatar_id", "full_name"]
    )

    nodes = ma.Nested(TestNodeSchema, many=True)

    @pre_load()
    def index_nodes(self, data, **kwargs):
        index = 0
        for node in data.get("nodes", []):
            node["index"] = index
            index += 1
            item_index = 0
            for item in node.get("items"):
                item["index"] = item_index
                item_index += 1
        return data

    @post_dump()
    def sorted_nodes(self, data, **kwargs):
        if data.get("nodes"):
            data["nodes"] = sorted(
                data.get("nodes", []), key=lambda node: node.get("index", 0)
            )
            for node in data["nodes"]:
                node["items"] = sorted(
                    node.get("items", []), key=lambda item: item.get("index", 0)
                )
        return data

    @validates_schema
    def validate_has_start_node(self, data, **kwargs):
        has_start_node = False
        for node in data.get("nodes", []):
            if node.start == True:
                has_start_node = True
                return
        if not has_start_node and len(data.get("nodes", [])) > 0:
            raise ValidationError("one start node needs to be defined")

    # each path needs to have a node with end == True


TEST_EDGES_SCHEMA = TestEdgeSchema(many=True)
TEST_ITEMS_SCHEMA = TestItemSchema(many=True)
TEST_VERSION_SCHEMA = TestVersionSchema()
TEST_VERSIONS_SCHEMA = TestVersionSchema(many=True)
