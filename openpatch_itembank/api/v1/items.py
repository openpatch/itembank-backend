import os
import requests
from flask import jsonify, request
from marshmallow import ValidationError
from openpatch_core.database import db
from openpatch_core.rabbitmq import rabbit
from openpatch_core.jwt import get_jwt_claims, jwt_required
from openpatch_itembank.api.v1 import api, errors
from openpatch_itembank.api.v1.schemas.item import ITEM_SCHEMA, ITEMS_SCHEMA
from openpatch_itembank.api.v1.schemas.item_version import (
    ITEM_VERSION_SCHEMA,
    ITEM_VERSIONS_SCHEMA,
)
from openpatch_itembank.api.v1.schemas.member import MEMBER_SCHEMA
from openpatch_itembank.models.item import Item
from openpatch_itembank.models.item_version import ItemVersion
from openpatch_itembank.models.test_version import TestItem
from openpatch_itembank.models.member import Member
from openpatch_itembank.models.privacy import Privacy
from sqlalchemy.exc import DataError, StatementError

BASE_URL = "/items"


@api.route(BASE_URL, methods=["POST", "GET"])
def items():
    if request.method == "POST":
        return post_items()
    return get_items()


@jwt_required
def post_items():
    """
    @api {post} /v1/items Post items
    @apiVersion 1.0.0
    @apiName PostItems
    @apiGroup Items

    @apiUse jwt

    @apiParam {String{1..255}} name Name
    @apiParam {String} private_description Private hidden description
    @apiParam {String} public_description Public description
    @apiParam {String} source_reference Reference to the original source
    @apiParam {Boolean} public Is the item visible to all user
    @apiParam {String{5}} language Country Code

    @apiSuccess {Number} item_id The item id.

    @apiUse errors_no_json
    @apiUse errors_invalid_json

    @apiPermission admin
    @apiPermission author
    """
    jwt_claims = get_jwt_claims()

    item_json = request.get_json()
    if not item_json:
        return errors.no_json()

    # item is created by current member
    member = Member.get_or_create(jwt_claims)

    try:
        item = ITEM_SCHEMA.load(item_json, session=db.session)
        item.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    db.session.add(item)
    db.session.commit()

    item_version = ItemVersion(
        latest=True,
        member=member,
        status="draft",
        item=item,
        version=1,
        tasks=[],
        version_message="Initial",
    )
    db.session.add(item_version)
    db.session.commit()

    if item.privacy == Privacy.public:
        rabbit.publish_activity(
            "create",
            {"id": item.id, "type": "item", "data": ITEM_SCHEMA.dump(item)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({"item_id": item.id})


def get_items():
    """
    @api {get} /v1/items Get items
    @apiVersion 1.0.0
    @apiName GetItems
    @apiGroup Items

    @apiUse jwt
    @apiUse elastic_query

    @apiSuccess {Object[]} items
    @apiSuccess {Number} items.id The item id.
    @apiSuccess {String{1..255}} items.name Name
    @apiSuccess {String} items.private_description Private hidden description
    @apiSuccess {String} items.public_description Public description
    @apiSuccess {String} items.source_reference Reference to the original source
    @apiSuccess {Boolean} items.public Is the item visible to all user
    @apiSuccess {String{5}} items.language Country Code
    """
    jwt_claims = get_jwt_claims(optional=True)

    item_query, count, page = Item.elastic_query(request.args.get("query", "{}"))

    if not jwt_claims:
        item_query = item_query.filter(Item.privacy == Privacy.public)
    elif jwt_claims.get("role") != "admin":
        # select only items created by the member and permitted items
        item_query = item_query.filter(
            db.or_(
                Item.privacy == Privacy.public, Item.member_id == jwt_claims.get("id")
            )
        )

    count = item_query.count()
    item_query = page(query=item_query)
    items = item_query.all()

    return jsonify({"items": ITEMS_SCHEMA.dump(items), "items_count": count}), 200


@api.route(BASE_URL + "/<item_id>", methods=["PUT", "GET", "DELETE"])
def item(item_id):
    if request.method == "PUT":
        return put_item(item_id)
    elif request.method == "DELETE":
        return delete_item(item_id)
    return get_item(item_id)


@jwt_required
def put_item(item_id):
    """
    @api {put} /v1/items/:id Put item
    @apiVersion 1.0.0
    @apiName PutItem
    @apiGroup Items

    @apiUse jwt

    @apiParam {Number} id Item id
    @apiParam {String{1..255}} name Name
    @apiParam {String} private_description Private hidden description
    @apiParam {String} public_description Public description
    @apiParam {String} source_reference Reference to the original source
    @apiParam {Boolean} public Is the item visible to all user
    @apiParam {String{5}} language Country Code

    @apiSuccess {Number} item_id Item id

    @apiUse errors_no_json
    @apiUse errors_invalid_json
    @apiUse errors_resource_not_found
    @apiUse errors_access_not_allowed

    @apiPermission admin
    @apiPermission author
    """
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    item_json = request.get_json()
    if not item_json:
        return errors.no_json()

    try:
        item = Item.query.get(item_id)
    except StatementError:
        return errors.resource_not_found()

    if not item:
        return errors.resource_not_found()

    if not item.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    if "versions" in item_json:
        del item_json["versions"]

    if "member" in item_json:
        del item_json["member"]

    if (
        item.used_in_tests > 0
        and item.privacy != Privacy.private
        and item_json["privacy"] == "private"
    ):
        del item_json["privacy"]

    try:
        result = ITEM_SCHEMA.load(
            item_json, session=db.session, instance=item, partial=True
        )
        result.member = member
    except ValidationError as e:
        return errors.invalid_json(e.messages)

    if item.privacy == Privacy.public:
        rabbit.publish_activity(
            "update",
            {"id": item.id, "type": "item", "data": ITEM_SCHEMA.dump(item)},
            MEMBER_SCHEMA.dump(member),
        )

    db.session.commit()

    return jsonify({"item_id": item.id}), 200


@jwt_required
def delete_item(item_id):
    """
    @api {delete} /v1/items/:id Delete item
    @apiVersion 1.0.0
    @apiName DeleteItem
    @apiGroup Items

    @apiUse jwt

    @apiParam {Number} id Item id

    @apiUse errors_resource_not_found
    @apiUse errors_access_not_allowed
    @apiUse errors_item_is_in_use

    @apiPermission admin
    @apiPermission author
    """
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    try:
        item = Item.query.get(item_id)
    except StatementError:
        return errors.resource_not_found()

    if not item:
        return errors.resource_not_found()

    if not item.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    # check if item is used in a test
    test_item = TestItem.query.filter_by(item=item).count()
    if test_item > 0:
        return errors.item_is_in_use()

    for version in item.versions:
        db.session.delete(version)

    db.session.delete(item)

    if item.privacy == Privacy.public:
        rabbit.publish_activity(
            "delete",
            {"id": item.id, "type": "item", "data": ITEM_SCHEMA.dump(item)},
            MEMBER_SCHEMA.dump(member),
        )

    db.session.commit()

    return jsonify({}), 200


def get_item(item_id):
    """
    @api {get} /v1/items/:id Get item
    @apiVersion 1.0.0
    @apiName GetItem
    @apiGroup Items

    @apiUse jwt

    @apiParam {Number} id Item id

    @apiSuccess {Object} item
    @apiSuccess {Number} item.id The item id.
    @apiSuccess {String{1..255}} item.name Name
    @apiSuccess {String} item.private_description Private hidden description
    @apiSuccess {String} item.public_description Public description
    @apiSuccess {String} item.source_reference Reference to the original source
    @apiSuccess {Boolean} item.public Is the item visible to all user
    @apiSuccess {String{5}} item.language Country Code

    @apiUse errors_resource_not_found
    @apiUse errors_access_not_allowed
    """
    jwt_claims = get_jwt_claims(optional=True)

    try:
        item = Item.query.get(item_id)
    except StatementError:
        return errors.resource_not_found()

    if not item:
        return errors.resource_not_found()

    if not item.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    return jsonify({"item": ITEM_SCHEMA.dump(item)})


@api.route(BASE_URL + "/<item_id>/remix", methods=["POST"])
@jwt_required
def remix_item(item_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    try:
        item = Item.query.get(item_id)
    except StatementError:
        return errors.resource_not_found()

    if not item:
        return errors.resource_not_found()

    if not item.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    new_item = Item(
        name=item.name + " (Remix)",
        public_description=item.public_description,
        public_description_text=item.public_description_text,
        source_reference=item.source_reference,
        privacy=item.privacy,
        language=item.language,
        member=member,
    )

    db.session.add(new_item)

    latest_item_version = ItemVersion.get_latest(item_id)
    if not latest_item_version:
        latest_item_version = ItemVersion.get_draft(item_id)
    if not latest_item_version:
        return errors.resource_not_found()
    new_item_version = latest_item_version.copy()
    new_item_version.member = member
    new_item_version.version = 1
    new_item_version.version_message = "remix"
    new_item_version.status = "draft"
    new_item_version.latest = True
    new_item_version.item = new_item

    item.remixes.append(new_item)

    db.session.add(new_item_version)
    db.session.commit()

    if new_item.privacy == Privacy.public:
        rabbit.publish_activity(
            "create:remix",
            {"id": item.id, "type": "item", "data": ITEM_SCHEMA.dump(item)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({"item_id": new_item.id})


@api.route(BASE_URL + "/versions/evaluate", methods=["POST"])
@jwt_required
def evaluate_item_version():
    """
    @api {get} /v1/items/versions/evaluate Evaluate Item Version
    @apiVersion 1.0.0
    @apiName PostVersionsEvaluate
    @apiGroup Items

    @apiUse jwt

    @apiParam {Object[]} tasks Array of tasks
    @apiParam {String} tasks.format_type Format Type
    @apiParam {Object} solutions Object of solutions ids need to match task array index

    @apiSuccess {Object} result.

    @apiUse errors_format_service_down
    @apiUse errors_invalid_json
    """
    evaluate_json = request.get_json()

    format_url = os.getenv("OPENPATCH_FORMAT_SERVICE")
    if not format_url:
        return errors.format_service_down()

    try:
        r = requests.post("%s/v1/evaluate-batch" % format_url, json=evaluate_json)
        if r.status_code >= 500:
            return errors.format_service_down()
        elif r.status_code >= 400:
            return errors.invalid_json(r.json().get("details"))

        return jsonify(r.json()), r.status_code, r.headers.items()

    except requests.ConnectionError:
        return errors.format_service_down()


@api.route(BASE_URL + "/<item_id>/versions/<version>", methods=["GET"])
def get_item_version(item_id, version):
    jwt_claims = get_jwt_claims(optional=True)

    item_version_query = ItemVersion.query.filter_by(item_id=item_id)
    if version == "latest":
        item_version_query = item_version_query.filter_by(latest=True)
    elif version == "draft":
        item_version_query = item_version_query.filter_by(status="draft")
    else:
        item_version_query = item_version_query.filter_by(version=version)

    item_version = item_version_query.first()

    if not item_version:
        return errors.resource_not_found()

    if not item_version.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    return jsonify({"item_version": ITEM_VERSION_SCHEMA.dump(item_version)})


@api.route(BASE_URL + "/<item_id>/versions", methods=["GET", "POST", "PUT"])
def item_versions(item_id):
    if request.method == "POST":
        return post_item_versions(item_id)
    elif request.method == "PUT":
        return put_item_versions(item_id)
    return get_item_versions(item_id)


def get_item_versions(item_id):
    """
    @api {get} /v1/items/:id/versions Get item versions
    @apiVersion 1.0.0
    @apiName GetItemVersions
    @apiGroup Items

    @apiUse jwt
    @apiUse elastic_query
    """
    jwt_claims = get_jwt_claims()

    try:
        item = Item.query.get(item_id)
    except StatementError:
        return errors.resource_not_found()

    if not item:
        return errors.resource_not_found()

    if not item.permitted_read(jwt_claims):
        return errors.access_not_allowed()

    item_versions_query, count, page = ItemVersion.elastic_query(
        request.args.get("query", "{}")
    )
    item_versions_query = item_versions_query.filter_by(item=item)
    count = item_versions_query.count()

    if jwt_claims.get("role") != "admin":
        # select only items created by the member and permitted items
        item_versions_query = item_versions_query.filter(
            db.or_(
                Item.privacy != Privacy.private, Item.member_id == jwt_claims.get("id")
            )
        )

    item_versions_query = page(query=item_versions_query)
    item_versions = item_versions_query.all()

    return jsonify(
        {
            "item_versions": ITEM_VERSIONS_SCHEMA.dump(item_versions),
            "item_versions_count": count,
        }
    )


@jwt_required
def put_item_versions(item_id):
    jwt_claims = get_jwt_claims()
    member = Member.get_or_create(jwt_claims)

    item_version_json = request.get_json()

    if not item_version_json:
        return errors.no_json()

    item_version_json["status"] = "draft"
    if "id" in item_version_json:
        del item_version_json["id"]
    if "version" in item_version_json:
        del item_version_json["version"]
    if "version_message" in item_version_json:
        del item_version_json["version_message"]
    if "latest" in item_version_json:
        del item_version_json["latest"]
    if "member" in item_version_json:
        del item_version_json["member"]

    item_version = ItemVersion.get_draft(item_id)
    if not item_version:
        return errors.resource_not_found()

    try:
        item = Item.query.get(item_id)
    except StatementError:
        return errors.resource_not_found()

    if not item.permitted_write(jwt_claims):
        return errors.access_not_allowed()

    for task in item_version_json.get("tasks", []):
        task["format_version"] = 1
        if "item_version" in task:
            del task["item_version"]

    try:
        with db.session.no_autoflush:
            result = ITEM_VERSION_SCHEMA.load(
                item_version_json,
                session=db.session,
                instance=item_version,
                partial=True,
            )
            result.member = member
    except ValidationError as e:
        db.session.rollback()
        return errors.invalid_json(e.messages)

    db.session.commit()

    if item.privacy == Privacy.public:
        rabbit.publish_activity(
            "update:version",
            {"id": item.id, "type": "item", "data": ITEM_SCHEMA.dump(item)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify({}), 200


@jwt_required
def post_item_versions(item_id):
    # aka new draft
    jwt_claims = get_jwt_claims()

    item_version_json = request.get_json()
    if not item_version_json:
        return errors.no_json()

    # item version is created by current member
    member = Member.get_or_create(jwt_claims)

    draft_item_version = ItemVersion.get_draft(item_id)
    latest_item_version = ItemVersion.get_latest(item_id)
    if not draft_item_version:
        return errors.resource_not_found()

    new_draft_item_version = draft_item_version.copy()
    draft_item_version.status = "pilot"
    draft_item_version.latest = True
    draft_item_version.version_message = item_version_json["version_message"]
    new_draft_item_version.version_message = "draft"
    new_draft_item_version.version = draft_item_version.version + 1
    new_draft_item_version.member = member

    if latest_item_version:
        latest_item_version.latest = False

    db.session.add(new_draft_item_version)
    db.session.commit()

    item = new_draft_item_version.item
    if item.privacy == Privacy.public:
        rabbit.publish_activity(
            "create:version",
            {"id": item.id, "type": "item", "data": ITEM_SCHEMA.dump(item)},
            MEMBER_SCHEMA.dump(member),
        )

    return jsonify(
        {
            "version": new_draft_item_version.version,
            "item_id": new_draft_item_version.item_id,
        }
    )


@api.route(BASE_URL + "/<item_id>/versions/<version>/status", methods=["PUT"])
@jwt_required
def put_item_version_status(item_id, version):
    json = request.get_json()
    if not json:
        return errors.no_json()

    status = json.get("status")

    if not status or status not in ["pilot", "ready", "faulty"]:
        return errors.invalid_json("Wrong status")

    jwt_claims = get_jwt_claims()

    item = ItemVersion.query.filter_by(item_id=item_id, version=version).first()

    if not item:
        return errors.resource_not_found()

    if not item.permitted_write(jwt_claims) or item.status == "draft":
        return errors.access_not_allowed()

    try:
        item.status = status
        db.session.commit()
    except DataError as e:
        db.session.rollback()
        return errors.invalid_json(e.messages)

    return jsonify({}), 200
