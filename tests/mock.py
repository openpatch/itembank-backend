from marshmallow import ValidationError
from openpatch_itembank.api.v1.schemas.item import ITEMS_SCHEMA
from openpatch_itembank.api.v1.schemas.item_version import ITEM_VERSIONS_SCHEMA
from openpatch_itembank.api.v1.schemas.member import MEMBERS_SCHEMA
from openpatch_itembank.api.v1.schemas.tag import TAGS_SCHEMA
from openpatch_itembank.api.v1.schemas.category import CATEGORIES_SCHEMA
from openpatch_itembank.api.v1.schemas.collection import COLLECTIONS_SCHEMA
from openpatch_itembank.api.v1.schemas.test import TESTS_SCHEMA
from openpatch_itembank.api.v1.schemas.test_version import (
    TEST_VERSIONS_SCHEMA,
    TEST_EDGES_SCHEMA,
)
from openpatch_core.database import db

members = [
    {"id": "250512d6-16e8-4161-8ac2-43501a7efe28"},
    {"id": "67a3eb02-a099-4930-bb9e-d6a91cb46a9c"},
    {"id": "749e10b4-9545-40ad-9f8a-8e41f08c817b"},
]

tags = [
    {"id": "oop"},
    {"id": "syntax"},
    {"id": "representations"},
    {"id": "functional programming"},
]

collections = [
    {
        "id": "51a458c2-96e4-4592-9223-1688c94908c0",
        "name": "oop",
        "member": members[0],
        "privacy": "public",
        "public_description": "Object-oriented Programming",
    },
    {
        "id": "03718df8-8fea-4da7-8244-0dbad7b7560b",
        "name": "syntax",
        "privacy": "private",
        "member": members[1],
        "public_description": "Syntax",
    },
    {
        "id": "28e14296-f2f8-4012-8274-35b3709b92eb",
        "name": "representations",
        "privacy": "private",
        "member": members[1],
        "public_description": "Mastering representations",
    },
    {
        "id": "06460b00-99d3-40f4-93cd-3be4bf4084c9",
        "name": "functional programming",
        "member": members[0],
        "privacy": "public",
        "public_description": "Functional Programming",
    },
]

categories = [
    {"id": "oop", "color": "FF0000"},
    {"id": "syntax", "color": "00FF00"},
    {"id": "representations", "color": "00000FF"},
    {"id": "functional programming", "color": "0F0F0F"},
]

items = [
    {
        "id": "0703434f-32ca-4551-a387-ef2620b30543",
        "member": members[0],
        "name": "Item 1",
        "tags": [tags[0]["id"]],
        "public_description": "External description",
        "source_reference": "Author 2016, Place",
        "privacy": "private",
        "language": "de",
    },
    {
        "id": "34ce5a47-3691-4cb3-974d-83e921523e6d",
        "member": members[1],
        "name": "Item 1",
        "public_description": "External description",
        "source_reference": "Author 2016, Place",
        "privacy": "private",
        "language": "de",
    },
    {
        "id": "f35966a4-b5df-490c-ad30-19d68a33b514",
        "member": members[1],
        "name": "Item 2",
        "public_description": "External description",
        "source_reference": "Author 2017, Place",
        "privacy": "public",
        "language": "de",
    },
    {
        "id": "5b8fa3b9-993e-4e95-8b72-329c3afbc4db",
        "member": members[2],
        "name": "Item 1",
        "public_description": "External description",
        "source_reference": "Author 1999, Place",
        "privacy": "public",
        "language": "en",
    },
    {
        "id": "6faa605c-1b01-4f22-849f-35fc485703a7",
        "member": members[2],
        "name": "Item 2",
        "public_description": "External description",
        "source_reference": "Author 1999, Place",
        "privacy": "public",
        "language": "en",
    },
    {
        "id": "d18d005e-1622-40c5-a4c4-5d27c33d325c",
        "member": members[0],
        "name": "Item 8",
    },
]

item_versions = [
    {
        "version": 1,
        "version_message": "First Version",
        "item": items[0]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "pilot",
        "latest": False,
    },
    {
        "version": 2,
        "version_message": "Second Version",
        "item": items[0]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "multiple-choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "pilot",
        "latest": True,
    },
    {
        "version": 3,
        "version_message": "Third Version",
        "item": items[0]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "multiple-choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "pilot",
        "latest": False,
    },
    {
        "item": items[0]["id"],
        "latest": False,
        "status": "draft",
        "tasks": [
            {
                "data": {"allow_multiple": True, "choices": ["Blue", "Teal", "Orange"]},
                "evaluation": {"choices": {"1": True, "0": True}, "skip": False},
                "format_type": "choice",
                "format_version": 1,
                "task": "What is blue?",
            },
            {
                "data": {"allow_multiple": True, "choices": ["Blue", "Teal", "Orange"]},
                "evaluation": {"choices": {"2": True}, "skip": False},
                "format_type": "choice",
                "format_version": 1,
                "task": "What is orange?",
            },
        ],
        "version": 4,
        "version_message": "draft",
    },
    {
        "version": 1,
        "version_message": "First Version",
        "item": items[1]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "parson-puzzle",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "pilot",
        "latest": False,
    },
    {
        "version": 2,
        "version_message": "Second Version",
        "item": items[1]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "multiple-choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "pilot",
        "latest": True,
    },
    {
        "version": 3,
        "version_message": "Third Version",
        "item": items[1]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "multiple-choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "draft",
        "latest": False,
    },
    {
        "version": 1,
        "version_message": "Draft",
        "item": items[2]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "multiple-choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "draft",
        "latest": False,
    },
    {
        "version": 1,
        "version_message": "Draft",
        "item": items[4]["id"],
        "tasks": [
            {
                "task": "Test",
                "format_type": "multiple-choice",
                "format_version": 1,
                "data": {},
                "evaluation": {},
            }
        ],
        "status": "draft",
        "latest": False,
    },
]

tests = [
    {
        "id": "0ae2379c-6a0c-49a5-9280-de7f671ecc7f",
        "name": "test 1",
        "member": members[0],
    },
    {
        "id": "9c6ff9ce-b30b-457f-a979-3a12da3a2fea",
        "name": "test 2",
        "privacy": "public",
        "member": members[1],
    },
]

test_versions = [
    {
        "id": "282b9055-e81e-4884-ab93-1dcb96e8d9ce",
        "member": members[0],
        "version": 1,
        "version_message": "Init",
        "status": "draft",
        "latest": True,
        "test": tests[0]["id"],
    },
    {
        "id": "e657f06f-b6ea-4001-abb7-1e0944d8d28e",
        "member": members[0],
        "version": 1,
        "version_message": "Init",
        "status": "pilot",
        "latest": True,
        "test": tests[1]["id"],
        "nodes": [
            {
                "id": "88ce2e36-01f6-4743-9cda-a6ca68065d13",
                "index": 0,
                "randomized": True,
                "flow": "jumpable",
                "start": True,
                "name": "Starting Point",
                "items": [
                    {"item_version": {"version": 1, "item_id": items[4]["id"]}},
                    {"item_version": {"version": 4, "item_id": items[0]["id"]}},
                ],
                "edges": [],
            },
            {
                "id": "72c0cdfc-4add-40ec-8371-6c2bbe59bc56",
                "index": 1,
                "end": True,
                "name": "Node 2",
                "items": [{"item_version": {"version": 3, "item_id": items[1]["id"]}}],
            },
        ],
    },
    {
        "id": "3a9da3a3-dade-4407-ae56-85564933a888",
        "member": members[0],
        "version": 2,
        "version_message": "Second draft with Nodes",
        "status": "draft",
        "test": tests[1]["id"],
        "nodes": [
            {
                "id": "0975c7ef-2f8d-48a1-8dd5-0cb693059c8f",
                "index": 0,
                "randomized": True,
                "flow": "jumpable",
                "start": True,
                "name": "Another Node",
                "items": [{"item_version": {"item_id": items[0]["id"], "version": 4}}],
                "edges": [],
            },
            {
                "id": "93bcdccd-a910-45ae-a783-fe94c8a2f058",
                "index": 1,
                "name": "Here we are again",
                "items": [{"item_version": {"item_id": items[1]["id"], "version": 2}}],
                "edges": [],
            },
        ],
    },
]

test_edges = [
    {
        "node_from": "93bcdccd-a910-45ae-a783-fe94c8a2f058",
        "node_to": "0975c7ef-2f8d-48a1-8dd5-0cb693059c8f",
        "threshold": 0,
    },
    {
        "node_from": "0975c7ef-2f8d-48a1-8dd5-0cb693059c8f",
        "node_to": "93bcdccd-a910-45ae-a783-fe94c8a2f058",
        "threshold": 1,
    },
    {
        "node_from": "0975c7ef-2f8d-48a1-8dd5-0cb693059c8f",
        "node_to": "0975c7ef-2f8d-48a1-8dd5-0cb693059c8f",
        "threshold": 0,
    },
    {
        "node_from": "88ce2e36-01f6-4743-9cda-a6ca68065d13",
        "node_to": "72c0cdfc-4add-40ec-8371-6c2bbe59bc56",
        "threshold": 1,
    },
    {
        "node_from": "88ce2e36-01f6-4743-9cda-a6ca68065d13",
        "node_to": "88ce2e36-01f6-4743-9cda-a6ca68065d13",
        "threshold": 0,
    },
]


def mock():
    result = MEMBERS_SCHEMA.load(members, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = TAGS_SCHEMA.load(tags, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = COLLECTIONS_SCHEMA.load(collections, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = CATEGORIES_SCHEMA.load(categories, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = ITEMS_SCHEMA.load(items, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = ITEM_VERSIONS_SCHEMA.load(item_versions, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = TESTS_SCHEMA.load(tests, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    with db.session.no_autoflush:
        result = TEST_VERSIONS_SCHEMA.load(test_versions, session=db.session)

    db.session.add_all(result)
    db.session.commit()

    result = TEST_EDGES_SCHEMA.load(test_edges, session=db.session)

    db.session.add_all(result)
    db.session.commit()
