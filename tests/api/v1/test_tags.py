from openpatch_itembank.models.tag import Tag
from tests import mock
from tests.base_test import BaseTest


class TagsTest(BaseTest):
    def test_post_tags_admin(self):
        tags = Tag.query.all()
        self.assertEqual(len(tags), len(mock.tags))

        new_tag = {"id": "oop 2"}

        response = self.client.post(
            "/v1/tags", headers=self.fake_headers["admin"], json=new_tag
        )
        self.assertEqual(response.status_code, 200)

        tags = Tag.query.all()
        self.assertEqual(len(tags), len(mock.tags) + 1)

    def test_post_tags_author(self):
        new_tag = {"id": "oop new"}

        response = self.client.post(
            "/v1/tags", headers=self.fake_headers["author"], json=new_tag
        )
        self.assertEqual(response.status_code, 200)

        tags = Tag.query.all()
        self.assertEqual(len(tags), len(mock.tags) + 1)

    def test_post_tags_user(self):
        new_tag = {"id": "oop new"}

        response = self.client.post(
            "/v1/tags", headers=self.fake_headers["user"], json=new_tag
        )
        self.assertEqual(response.status_code, 200)

        tags = Tag.query.all()
        self.assertEqual(len(tags), len(mock.tags) + 1)

    def test_get_tags_admin(self):
        response = self.client.get("/v1/tags", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("tags", response.get_json())
        self.assertEqual(len(response.get_json().get("tags")), len(mock.tags))

    def test_get_tags_author(self):
        response = self.client.get("/v1/tags", headers=self.fake_headers["author"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("tags", response.get_json())
        self.assertEqual(len(response.get_json().get("tags")), len(mock.tags))

    def test_delete_tag_user(self):
        response = self.client.delete(
            f"/v1/tags/{mock.tags[0]['id']}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 403)

    def test_delete_tag_author(self):
        tags = Tag.query.all()
        self.assertEqual(len(tags), 4)

        response = self.client.delete(
            f"/v1/tags/{mock.tags[0]['id']}", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 403)

        tags = Tag.query.all()
        self.assertEqual(len(tags), 4)

    def test_delete_tag_admin(self):
        response = self.client.delete(
            f"/v1/tags/{mock.tags[0]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)

        tags = Tag.query.all()
        self.assertEqual(len(tags), 3)

    def test_get_tag_user(self):
        response = self.client.get(
            f"/v1/tags/{mock.tags[0]['id']}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)

    def test_get_tag_author(self):
        response = self.client.get(
            f"/v1/tags/{mock.tags[0]['id']}", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 200)

    def test_get_tag_admin(self):
        response = self.client.get(
            f"/v1/tags/{mock.tags[0]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)

    def test_put_tag_user(self):
        update_tag = {"id": "oop new"}

        response = self.client.put(
            f"/v1/tags/{mock.tags[0]['id']}",
            headers=self.fake_headers["user"],
            json=update_tag,
        )
        self.assertEqual(response.status_code, 403)

    def test_put_tag_author(self):
        update_tag = {"id": "oop new"}

        response = self.client.put(
            f"/v1/tags/{mock.tags[0]['id']}",
            headers=self.fake_headers["author"],
            json=update_tag,
        )
        self.assertEqual(response.status_code, 403)
        tag = Tag.query.get(mock.tags[0]["id"])
        self.assertNotEqual(tag.id, update_tag["id"])

