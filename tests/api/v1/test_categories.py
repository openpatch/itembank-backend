from openpatch_itembank.models.category import Category
from tests import mock
from tests.base_test import BaseTest


class CategoriesTest(BaseTest):
    def test_post_categories_admin(self):
        categories = Category.query.all()
        self.assertEqual(len(categories), len(mock.categories))

        new_category = {"id": "oop 2", "color": "FF0000"}

        response = self.client.post(
            "/v1/categories", headers=self.fake_headers["admin"], json=new_category
        )
        self.assertEqual(response.status_code, 200)

        categories = Category.query.all()
        self.assertEqual(len(categories), len(mock.categories) + 1)

    def test_post_categories_author(self):
        new_category = {"name": "id", "color": "FF0000"}

        response = self.client.post(
            "/v1/categories", headers=self.fake_headers["author"], json=new_category
        )
        self.assertEqual(response.status_code, 403)

        categories = Category.query.all()
        self.assertEqual(len(categories), len(mock.categories))

    def test_post_categories_user(self):
        new_category = {"name": "id", "color": "FF0000"}

        response = self.client.post(
            "/v1/categories", headers=self.fake_headers["user"], json=new_category
        )
        self.assertEqual(response.status_code, 403)

        categories = Category.query.all()
        self.assertEqual(len(categories), len(mock.categories))

    def test_get_categories(self):
        response = self.client.get("/v1/categories", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("categories", response.get_json())
        self.assertEqual(
            len(response.get_json().get("categories")), len(mock.categories)
        )

    def test_delete_category_user(self):
        response = self.client.delete(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 403)

    def test_delete_category_admin(self):
        response = self.client.delete(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)

        categories = Category.query.all()
        self.assertEqual(len(categories), 3)

    def test_get_category_user(self):
        response = self.client.get(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)

    def test_get_category_author(self):
        response = self.client.get(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)

    def test_get_category_admin(self):
        response = self.client.get(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)

    def test_put_category_user(self):
        update_category = {"id": "oop new"}

        response = self.client.put(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["user"],
            json=update_category,
        )
        self.assertEqual(response.status_code, 403)

    def test_put_category_author(self):
        update_category = {"id": "oop new"}

        response = self.client.put(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["author"],
            json=update_category,
        )

    def test_put_category_admin(self):
        update_category = {"id": "oop new"}

        response = self.client.put(
            f"/v1/categories/{mock.categories[0]['id']}",
            headers=self.fake_headers["admin"],
            json=update_category,
        )
        self.assertEqual(response.status_code, 200)
        self.assertIsNone(Category.query.get("oop"))
        self.assertIsNotNone(Category.query.get("oop new"))
