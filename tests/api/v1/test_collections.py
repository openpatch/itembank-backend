from openpatch_itembank.models.collection import Collection
from tests import mock
from tests.base_test import BaseTest


class CollectionsTest(BaseTest):
    def test_post_collections_admin(self):
        collections = Collection.query.all()
        self.assertEqual(len(collections), len(mock.collections))

        new_collection = {
            "name": "oop 2",
            "privacy": "private",
            "public_description": "Once again",
        }

        response = self.client.post(
            "/v1/collections", headers=self.fake_headers["admin"], json=new_collection
        )
        self.assertEqual(response.status_code, 200)

        collections = Collection.query.all()
        self.assertEqual(len(collections), len(mock.collections) + 1)

    def test_post_collections_author(self):
        new_collection = {
            "name": "oop",
            "privacy": "private",
            "public_description": "Once again",
        }

        response = self.client.post(
            "/v1/collections", headers=self.fake_headers["author"], json=new_collection
        )
        self.assertEqual(response.status_code, 200)

        collections = Collection.query.all()
        self.assertEqual(len(collections), len(mock.collections) + 1)

    def test_post_collections_user(self):
        new_collection = {
            "name": "oop",
            "privacy": "private",
            "public_description": "Once again",
        }

        response = self.client.post(
            "/v1/collections", headers=self.fake_headers["user"], json=new_collection
        )
        self.assertEqual(response.status_code, 200)

        collections = Collection.query.all()
        self.assertEqual(len(collections), len(mock.collections) + 1)

    def test_get_collections_admin(self):
        response = self.client.get(
            "/v1/collections", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("collections", response.get_json())
        self.assertEqual(
            len(response.get_json().get("collections")), len(mock.collections)
        )

    def test_get_collections_author(self):
        response = self.client.get(
            "/v1/collections", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("collections", response.get_json())
        self.assertEqual(len(response.get_json().get("collections")), 4)

    def test_get_collections_user(self):
        response = self.client.get("/v1/collections", headers=self.fake_headers["user"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("collections", response.get_json())
        self.assertEqual(len(response.get_json().get("collections")), 2)

    def test_delete_collection_user(self):
        response = self.client.delete(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["user"],
        )
        self.assertNotEqual(response.status_code, 200)

    def test_delete_collection_author(self):
        collections = Collection.query.all()
        self.assertEqual(len(collections), 4)

        response = self.client.delete(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["author"],
        )
        self.assertNotEqual(response.status_code, 200)

        collections = Collection.query.all()
        self.assertEqual(len(collections), 4)

        response = self.client.delete(
            f"/v1/collections/{mock.collections[1]['id']}",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)

        collections = Collection.query.all()
        self.assertEqual(len(collections), 3)

    def test_delete_collection_admin(self):
        response = self.client.delete(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)

        collections = Collection.query.all()
        self.assertEqual(len(collections), 3)

    def test_get_collection_user(self):
        response = self.client.get(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f"/v1/collections/{mock.collections[1]['id']}",
            headers=self.fake_headers["user"],
        )
        self.assertNotEqual(response.status_code, 200)

    def test_get_collection_author(self):
        response = self.client.get(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f"/v1/collections/{mock.collections[1]['id']}",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)

    def test_get_collection_admin(self):
        response = self.client.get(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)

    def test_put_collection_user(self):
        update_collection = {"name": "oop new"}

        response = self.client.put(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["user"],
            json=update_collection,
        )
        self.assertNotEqual(response.status_code, 200)
        collection = Collection.query.get(mock.collections[0]["id"])
        self.assertNotEqual(collection.name, update_collection["name"])

    def test_put_collection_author(self):
        update_collection = {"name": "oop new"}

        response = self.client.put(
            f"/v1/collections/{mock.collections[0]['id']}",
            headers=self.fake_headers["author"],
            json=update_collection,
        )
        self.assertNotEqual(response.status_code, 200)
        collection = Collection.query.get(mock.collections[0]["id"])
        self.assertNotEqual(collection.name, update_collection["name"])

        response = self.client.put(
            f"/v1/collections/{mock.collections[1]['id']}",
            headers=self.fake_headers["author"],
            json=update_collection,
        )
        self.assertEqual(response.status_code, 200)

    def test_put_collection_admin(self):
        pass
