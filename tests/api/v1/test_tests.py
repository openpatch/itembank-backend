from flask import json
from tests import mock
from openpatch_itembank.models.test import Test
from openpatch_itembank.models.test_version import TestVersion
from tests.base_test import BaseTest
from tests import mock


class TestTests(BaseTest):
    def test_post_tests_user(self):
        new_test = {
            "name": "Test 2",
            "public_description": "Public",
            "privacy": "private",
        }

        old_tests = Test.query.count()
        response = self.client.post(
            "/v1/tests", headers=self.fake_headers["user"], json=new_test
        )
        self.assertEqual(response.status_code, 200)

        new_tests = Test.query.count()
        self.assertEqual(old_tests + 1, new_tests)

    def test_post_tests_admin(self):
        new_test = {
            "name": "Test 2",
            "public_description": "Public",
            "privacy": "private",
        }

        old_tests = Test.query.count()
        response = self.client.post(
            "/v1/tests", headers=self.fake_headers["admin"], json=new_test
        )
        self.assertEqual(response.status_code, 200)

        new_tests = Test.query.count()
        self.assertEqual(old_tests + 1, new_tests)

    def test_get_tests_user(self):
        response = self.client.get("/v1/tests", headers=self.fake_headers["user"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("tests", response.json)
        self.assertEqual(len(response.json.get("tests")), len(mock.tests) - 1)

    def test_get_tests_author(self):
        response = self.client.get("/v1/tests", headers=self.fake_headers["author"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("tests", response.json)
        self.assertEqual(len(response.json.get("tests")), len(mock.tests) - 1)

    def test_get_tests_admin(self):
        response = self.client.get("/v1/tests", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)
        self.assertIn("tests", response.json)
        self.assertEqual(len(response.json.get("tests")), len(mock.tests))

        # test elastic query
        query = {}
        response = self.client.get(
            "/v1/tests?query={}".format(json.dumps(query)),
            headers=self.fake_headers["admin"],
        )

    def test_put_test_user(self):
        test_json = {"public_description": "Hi there!"}
        response = self.client.put(
            f"/v1/tests/{mock.tests[0]['id']}",
            headers=self.fake_headers["user"],
            json=test_json,
        )
        self.assertNotEqual(response.status_code, 200)

    def test_put_test_author(self):
        test_json = {"public_description": "Hi there!"}
        response = self.client.put(
            f"/v1/tests/{mock.tests[0]['id']}",
            headers=self.fake_headers["author"],
            json=test_json,
        )
        self.assertNotEqual(response.status_code, 200)

    def test_put_test_admin(self):
        test_json = {"public_description": "Hi there!"}
        response = self.client.put(
            f"/v1/tests/{mock.tests[0]['id']}",
            headers=self.fake_headers["admin"],
            json=test_json,
        )
        self.assertEqual(response.status_code, 200)

        test = Test.query.get(mock.tests[0]["id"])
        self.assertEqual(test.public_description, "Hi there!")

    def test_delete_test_user(self):
        response = self.client.delete(
            f"/v1/tests/{mock.tests[0]['id']}", headers=self.fake_headers["user"]
        )
        self.assertNotEqual(response.status_code, 200)
        test = Test.query.get(mock.tests[0]["id"])
        self.assertIsNotNone(test)

    def test_delete_test_author(self):
        response = self.client.delete(
            f"/v1/tests/{mock.tests[0]['id']}", headers=self.fake_headers["author"]
        )
        self.assertNotEqual(response.status_code, 200)
        test = Test.query.get(mock.tests[0]["id"])
        self.assertIsNotNone(test)

        response = self.client.delete(
            f"/v1/tests/{mock.tests[1]['id']}", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 200)
        test = Test.query.get(mock.tests[1]["id"])
        self.assertIsNone(test)

    def test_delete_test_admin(self):
        response = self.client.delete(
            f"/v1/tests/{mock.tests[0]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
        test = Test.query.get(mock.tests[0]["id"])
        self.assertIsNone(test)

        response = self.client.delete(
            f"/v1/tests/{mock.tests[1]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
        test = Test.query.get(mock.tests[1]["id"])
        self.assertIsNone(test)

    def test_remix_test_user(self):
        old_test = Test.query.get(mock.tests[1]["id"])
        response = self.client.post(
            f"/v1/tests/{mock.tests[1]['id']}/remix",
            headers=self.fake_headers["author"],
        )
        old_test_after = Test.query.get(mock.tests[1]["id"])
        self.assertEqual(len(old_test_after.remixes), 1)
        self.assertEqual(old_test, old_test_after)
        self.assertEqual(response.status_code, 200)
        self.assertIn("test_id", response.json)
        new_test = Test.query.get(response.json["test_id"])
        self.assertEqual(old_test_after.remixes[0], new_test)

        self.assertEqual(new_test.name, old_test.name + " (Remix)")
        new_test_version = TestVersion.get_latest(new_test.id)
        self.assertEqual(new_test_version.version, 1)
        self.assertEqual(new_test_version.status, "draft")
        self.assertEqual(new_test_version.version_message, "remix")

    def test_get_test_user(self):
        response = self.client.get(
            f"/v1/tests/{mock.tests[0]['id']}", headers=self.fake_headers["user"]
        )
        self.assertNotEqual(response.status_code, 200)

        response = self.client.get(
            f"/v1/tests/{mock.tests[1]['id']}", headers=self.fake_headers["user"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("test", response.json)

    def test_get_test_author(self):
        response = self.client.get(
            f"/v1/tests/{mock.tests[0]['id']}", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 403)

        response = self.client.get(
            f"/v1/tests/{mock.tests[1]['id']}", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("test", response.json)

    def test_get_test_admin(self):
        response = self.client.get(
            f"/v1/tests/{mock.tests[0]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)

        response = self.client.get(
            f"/v1/tests/{mock.tests[1]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("test", response.json)

    def test_get_test_version_user(self):
        response = self.client.get(
            f"/v1/tests/{mock.tests[0]['id']}/versions/1",
            headers=self.fake_headers["user"],
        )
        self.assertNotEqual(response.status_code, 200)

        response = self.client.get(
            f"/v1/tests/{mock.tests[1]['id']}/versions/1",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("test_version", response.json)

    def test_get_test_version_author(self):
        response = self.client.get(
            f"/v1/tests/{mock.tests[0]['id']}/versions/1",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 403)

        response = self.client.get(
            f"/v1/tests/{mock.tests[1]['id']}/versions/1",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)

    def test_get_test_version_admin(self):
        response = self.client.get(
            f"/v1/tests/{mock.tests[0]['id']}/versions/1",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("test_version", response.json)

        response = self.client.get(
            f"/v1/tests/{mock.tests[1]['id']}/versions/1",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("test_version", response.json)

    def test_put_test_version_user(self):
        test_version_json = {
            "nodes": [
                {
                    "randomized": True,
                    "jumpable": True,
                    "name": "Hi",
                    "needs_to_be_correct": True,
                    "items": [
                        {"item": mock.items[0]["id"], "version": 1},
                        {"item": mock.items[1]["id"], "version": 2},
                    ],
                    "edges": [{"node": 1, "threshold": 1, "node": 0, "threshold": 0}],
                },
                {
                    "randomized": False,
                    "jumpable": False,
                    "name": "hu",
                    "items": [
                        {"item": mock.items[0]["id"], "version": 2},
                        {"item": mock.items[2]["id"], "version": 1},
                    ],
                    "edges": [],
                },
            ]
        }
        response = self.client.put(
            f"/v1/tests/{mock.tests[0]['id']}/versions",
            headers=self.fake_headers["user"],
            json=test_version_json,
        )
        self.assertNotEqual(response.status_code, 200)

    def test_put_test_version_author(self):
        test_version_json = {
            "nodes": [
                {
                    "randomized": True,
                    "jumpable": True,
                    "name": "hi",
                    "needs_to_be_correct": True,
                    "items": [
                        {"item": mock.items[0]["id"], "version": 1},
                        {"item": mock.items[1]["id"], "version": 2},
                    ],
                    "edges": [{"node_from": 1, "threshold": 1, "node_to": 0}],
                },
                {
                    "randomized": False,
                    "jumpable": False,
                    "name": "hu",
                    "items": [
                        {"item": mock.items[0]["id"], "version": 2},
                        {"item": mock.items[2]["id"], "version": 1},
                    ],
                    "edges": [],
                },
            ]
        }
        response = self.client.put(
            f"/v1/tests/{mock.tests[0]['id']}/versions",
            headers=self.fake_headers["author"],
            json=test_version_json,
        )
        self.assertNotEqual(response.status_code, 200)

    def test_put_test_version_admin(self):
        test_version_json = {
            "nodes": [
                {
                    "id": 0,
                    "randomized": True,
                    "flow": "linear",
                    "start": True,
                    "name": "Hi",
                    "needs_to_be_correct": True,
                    "items": [
                        {
                            "item_version": {
                                "version": 1,
                                "item_id": mock.items[0]["id"],
                            }
                        },
                        {
                            "item_version": {
                                "version": 1,
                                "item_id": mock.items[1]["id"],
                            }
                        },
                    ],
                    "edges": [
                        {"node_from": 1, "node_to": 0, "threshold": 1},
                        {"node_from": 0, "node_to": 1, "threshold": 0},
                    ],
                },
                {
                    "randomized": False,
                    "flow": "adaptive",
                    "item_limit": 2,
                    "id": 1,
                    "name": "hu",
                    "items": [
                        {
                            "item_version": {
                                "item_id": mock.items[1]["id"],
                                "version": 1,
                            }
                        },
                        {
                            "item_version": {
                                "version": 1,
                                "item_id": mock.items[0]["id"],
                            }
                        },
                    ],
                    "edges": [],
                },
            ]
        }
        response = self.client.put(
            f"/v1/tests/{mock.tests[0]['id']}/versions",
            headers=self.fake_headers["admin"],
            json=test_version_json,
        )
        self.assertNotEqual(response.status_code, 200)

        test_version = TestVersion.get_draft(mock.tests[0]["id"])

        self.assertNotEqual(len(test_version.nodes), 2)

