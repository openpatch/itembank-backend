import os
from flask import json as fjson
from openpatch_itembank.models.item import Item
from openpatch_itembank.models.item_version import ItemVersion
from openpatch_itembank.models.privacy import Privacy
from tests.base_test import BaseTest
from tests import mock
import httpretty

format_validate_url = "%s/v1/validate" % os.getenv("OPENPATCH_FORMAT_SERVICE")
format_evaluate_url = "%s/v1/evaluate-batch" % os.getenv("OPENPATCH_FORMAT_SERVICE")


class TestItems(BaseTest):
    def test_post_items_user(self):
        new_item = {
            "name": "Item 2",
            "public_description": "Test",
            "source_reference": "Reference",
            "privacy": "private",
            "language": "de",
        }

        old_items = Item.query.count()
        response = self.client.post(
            "/v1/items", headers=self.fake_headers["user"], json=new_item
        )
        self.assertEqual(response.status_code, 200)

        new_items = Item.query.count()
        self.assertEqual(old_items + 1, new_items)

    def test_post_items_author(self):
        new_item = {
            "name": "Item 2",
            "public_description": "Test",
            "source_reference": "Reference",
            "privacy": "private",
            "language": "de",
        }

        old_items = Item.query.count()
        response = self.client.post(
            "/v1/items", headers=self.fake_headers["author"], json=new_item
        )

        self.assertEqual(response.status_code, 200)

        new_items = Item.query.count()
        self.assertEqual(old_items + 1, new_items)

    def test_post_items_admin(self):
        new_item = {
            "name": "Item 2",
            "public_description": "Test",
            "source_reference": "Reference",
            "privacy": "private",
            "language": "de",
        }

        old_items = Item.query.count()
        response = self.client.post(
            "/v1/items", headers=self.fake_headers["admin"], json=new_item
        )
        self.assertEqual(response.status_code, 200)

        new_items = Item.query.count()
        self.assertEqual(old_items + 1, new_items)

    def test_get_items_user(self):
        response = self.client.get("/v1/items", headers=self.fake_headers["user"])
        self.assertEqual(response.status_code, 200)

        json = response.get_json()
        self.assertIn("items", json)

        items = json.get("items")
        self.assertEqual(len(items), 3)
        self.assertIsNotNone(items[0]["member"])

    def test_get_items_author(self):
        response = self.client.get("/v1/items", headers=self.fake_headers["author"])
        self.assertEqual(response.status_code, 200)

        json = response.get_json()
        self.assertIn("items", json)

        items = json.get("items")
        self.assertEqual(len(items), 4)

    def test_get_items_admin(self):
        response = self.client.get("/v1/items", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)

        json = response.get_json()
        self.assertIn("items", json)

        items = json.get("items")
        self.assertEqual(len(items), 6)

        filter_query = {"and": {"name": {"like": "Item%"}}}

        limit = 2
        offset = 0
        sort = {"name": "asc", "member_id": "desc"}

        query = {"limit": limit, "offset": offset, "sort": sort, "filter": filter_query}

        response = self.client.get(
            "/v1/items?query={}".format(fjson.dumps(query)),
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)

        json = response.get_json()
        self.assertIn("items", json)

        items = json.get("items")
        self.assertEqual(len(items), 2)

    def test_get_items_for_tag_admin(self):
        response = self.client.get("/v1/items", headers=self.fake_headers["admin"])
        self.assertEqual(response.status_code, 200)

        json = response.get_json()
        self.assertIn("items", json)

        items = json.get("items")
        self.assertEqual(len(items), 6)

        filter_query = {"tags.id": "oop"}

        limit = 5
        offset = 0
        sort = {"name": "asc", "member_id": "desc"}

        query = {"limit": limit, "offset": offset, "sort": sort, "filter": filter_query}

        response = self.client.get(
            "/v1/items?query={}".format(fjson.dumps(query)),
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)

        json = response.get_json()
        self.assertIn("items", json)

        items = json.get("items")
        self.assertEqual(len(items), 1)

    def test_put_item_user(self):
        update_item = {"name": "new name"}
        response = self.client.put(
            f"/v1/items/{mock.items[0]['id']}",
            headers=self.fake_headers["user"],
            json=update_item,
        )

        self.assertNotEqual(response.status_code, 200)

        item = Item.query.get(mock.items[0]["id"])
        self.assertNotEqual(update_item["name"], item.name)

    def test_put_item_author(self):
        update_item = {"name": "new name"}
        response = self.client.put(
            f"/v1/items/{mock.items[4]['id']}",
            headers=self.fake_headers["author"],
            json=update_item,
        )

        self.assertNotEqual(response.status_code, 200)

        item = Item.query.get(mock.items[4]["id"])
        self.assertNotEqual(update_item["name"], item.name)

        response = self.client.put(
            f"/v1/items/{mock.items[2]['id']}",
            headers=self.fake_headers["author"],
            json=update_item,
        )

        self.assertEqual(response.status_code, 200)

        item = Item.query.get(mock.items[2]["id"])
        self.assertEqual(update_item["name"], item.name)

    def test_put_item_admin(self):
        update_item = {"name": "new name", "privacy": "private"}
        response = self.client.put(
            f"/v1/items/{mock.items[4]['id']}",
            headers=self.fake_headers["admin"],
            json=update_item,
        )

        self.assertEqual(response.status_code, 200)

        item = Item.query.get(mock.items[4]["id"])
        self.assertEqual(update_item["name"], item.name)
        self.assertTrue(item.privacy == Privacy.public)

        response = self.client.put(
            f"/v1/items/{mock.items[2]['id']}",
            headers=self.fake_headers["admin"],
            json=update_item,
        )

        self.assertEqual(response.status_code, 200)

        item = Item.query.get(mock.items[2]["id"])
        self.assertEqual(update_item["name"], item.name)

    def test_delete_item_user(self):
        old_items = Item.query.count()
        response = self.client.delete(
            f"/v1/items/{mock.items[4]['id']}", headers=self.fake_headers["user"]
        )
        self.assertNotEqual(response.status_code, 200)
        new_items = Item.query.count()
        self.assertEqual(old_items, new_items)

    def test_delete_item_author(self):
        old_items = Item.query.count()
        response = self.client.delete(
            f"/v1/items/{mock.items[4]['id']}", headers=self.fake_headers["author"]
        )
        self.assertNotEqual(response.status_code, 200)
        new_items = Item.query.count()
        self.assertEqual(old_items, new_items)

        old_items = Item.query.count()
        response = self.client.delete(
            f"/v1/items/{mock.items[2]['id']}", headers=self.fake_headers["author"]
        )
        self.assertEqual(response.status_code, 200)
        new_items = Item.query.count()
        self.assertEqual(old_items - 1, new_items)

    def test_delete_item_admin(self):
        old_items = Item.query.count()
        response = self.client.delete(
            f"/v1/items/{mock.items[4]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 400)

        old_items = Item.query.count()
        response = self.client.delete(
            f"/v1/items/{mock.items[2]['id']}", headers=self.fake_headers["admin"]
        )
        self.assertEqual(response.status_code, 200)
        new_items = Item.query.count()
        self.assertEqual(old_items - 1, new_items)

    def test_remix_item_user(self):
        old_item = Item.query.get(mock.items[2]["id"])
        response = self.client.post(
            f"/v1/items/{mock.items[2]['id']}/remix",
            headers=self.fake_headers["author"],
        )
        old_item_after = Item.query.get(mock.items[2]["id"])
        self.assertEqual(len(old_item_after.remixes), 1)
        self.assertEqual(old_item, old_item_after)
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_id", response.json)
        new_item = Item.query.get(response.json["item_id"])
        self.assertEqual(old_item_after.remixes[0], new_item)

        self.assertEqual(new_item.name, old_item.name + " (Remix)")
        new_item_version = ItemVersion.get_latest(new_item.id)
        self.assertEqual(new_item_version.version, 1)
        self.assertEqual(new_item_version.status, "draft")
        self.assertEqual(new_item_version.version_message, "remix")

    def test_get_item_version_user(self):
        response = self.client.get(
            f"/v1/items/{mock.items[4]['id']}/versions/1",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_version", response.get_json())

    def test_get_item_version_author(self):
        response = self.client.get(
            f"/v1/items/{mock.items[0]['id']}/versions/1",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_version", response.get_json())

        response = self.client.get(
            f"/v1/items/{mock.items[1]['id']}/versions/1",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_version", response.get_json())

        response = self.client.get(
            f"/v1/items/{mock.items[4]['id']}/versions/1",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_version", response.get_json())

    def test_get_item_version_admin(self):
        response = self.client.get(
            f"/v1/items/{mock.items[0]['id']}/versions/1",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_version", response.get_json())

    def test_put_item_versions_user(self):
        update_item_version = {"tasks": [{"format_type": "new", "format_version": 1}]}
        response = self.client.put(
            f"/v1/items/{mock.items[0]['id']}/versions",
            headers=self.fake_headers["user"],
            json=update_item_version,
        )
        self.assertNotEqual(response.status_code, 200)

        iv = ItemVersion.query.filter_by(
            item_id=mock.items[0]["id"], status="draft"
        ).first()
        self.assertNotEqual(
            update_item_version["tasks"][0]["format_type"], iv.tasks[0].format_type
        )

    def test_put_item_versions_admin(self):
        httpretty.register_uri(
            httpretty.POST, format_validate_url, body='{"correct": true}'
        )

        update_item_version = {"tasks": [{"format_type": "new", "format_version": 1}]}
        response = self.client.put(
            f"/v1/items/{mock.items[0]['id']}/versions",
            headers=self.fake_headers["admin"],
            json=update_item_version,
        )
        self.assertEqual(response.status_code, 200)

        iv = ItemVersion.query.filter_by(
            item_id=mock.items[0]["id"], status="draft"
        ).first()
        self.assertEqual(
            update_item_version["tasks"][0]["format_type"], iv.tasks[0].format_type
        )
        self.assertEqual(iv.status, "draft")

    def test_post_item_versions_validate(self):
        httpretty.register_uri(
            httpretty.POST, format_validate_url, body='{"correct": false}'
        )

        item_version = {"version_message": "new version"}
        response = self.client.post(
            f"/v1/items/{mock.items[0]['id']}/versions",
            headers=self.fake_headers["user"],
            json=item_version,
        )

        self.assertEqual(response.status_code, 200)

    def test_post_item_version_evaluate(self):
        httpretty.register_uri(
            httpretty.POST, format_evaluate_url, body='[{"correct": false}]'
        )

        evaluate = {"tasks": [{}], "solutions": [{}]}

        response = self.client.post(
            "v1/items/versions/evaluate",
            headers=self.fake_headers["admin"],
            json=evaluate,
        )

        self.assertEqual(response.status_code, 200)
        self.assertEqual(response.json[0].get("correct"), False)

    def test_post_item_versions_admin(self):
        httpretty.register_uri(
            httpretty.POST, format_validate_url, body='{"correct": true}'
        )

        item_version = {"version_message": "new version"}
        old_iv = ItemVersion.query.count()
        response = self.client.post(
            f"/v1/items/{mock.items[0]['id']}/versions",
            headers=self.fake_headers["admin"],
            json=item_version,
        )
        self.assertEqual(response.status_code, 200)
        new_iv = ItemVersion.query.count()
        self.assertEqual(old_iv + 1, new_iv)

        draft_iv = ItemVersion.query.filter_by(
            item_id=mock.items[0]["id"], status="draft"
        ).one()
        latest_iv = ItemVersion.query.filter_by(
            item_id=mock.items[0]["id"], latest=True
        ).one()
        self.assertEqual(draft_iv.version_message, "draft")
        self.assertEqual(latest_iv.version_message, item_version["version_message"])

    def test_get_item_versions_user(self):
        response = self.client.get(
            f"/v1/items/{mock.items[4]['id']}/versions",
            headers=self.fake_headers["user"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_versions", response.get_json())

    def test_get_item_versions_author(self):
        response = self.client.get(
            f"/v1/items/{mock.items[0]['id']}/versions",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_versions", response.get_json())

        response = self.client.get(
            f"/v1/items/{mock.items[1]['id']}/versions",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_versions", response.get_json())

        response = self.client.get(
            f"/v1/items/{mock.items[4]['id']}/versions",
            headers=self.fake_headers["author"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_versions", response.get_json())

    def test_get_item_versions_admin(self):
        response = self.client.get(
            f"/v1/items/{mock.items[0]['id']}/versions",
            headers=self.fake_headers["admin"],
        )
        self.assertEqual(response.status_code, 200)
        self.assertIn("item_versions", response.get_json())

    def test_put_item_version_status_user(self):
        update_status = {"status": "ready"}
        response = self.client.put(
            f"/v1/items/{mock.items[0]['id']}/versions/1/status",
            headers=self.fake_headers["user"],
            json=update_status,
        )
        self.assertNotEqual(response.status_code, 200)

        iv = ItemVersion.query.filter_by(item_id=mock.items[0]["id"], version=1).one()
        self.assertNotEqual(update_status["status"], iv.status)

    def test_put_item_version_status_admin(self):
        update_status = {"status": "ready"}
        response = self.client.put(
            f"/v1/items/{mock.items[0]['id']}/versions/1/status",
            headers=self.fake_headers["admin"],
            json=update_status,
        )
        self.assertEqual(response.get_json(), {})
        self.assertEqual(response.status_code, 200)

        iv = ItemVersion.query.filter_by(item_id=mock.items[0]["id"], version=1).one()
        self.assertEqual(update_status["status"], iv.status)
