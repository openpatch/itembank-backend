import unittest
from openpatch_itembank.utils.draft import to_text


class TestDraft(unittest.TestCase):
    def test_to_text(self):
        data = {
            "blocks": [
                {
                    "key": "dmcg5",
                    "text": "Hallo",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "8ih59",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "6tu7h",
                    "text": "Hallo und so weiter",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "dli9i",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
                {
                    "key": "dta4h",
                    "text": "",
                    "type": "unstyled",
                    "depth": 0,
                    "inline_style_ranges": [],
                    "entity_ranges": [],
                    "data": {},
                },
            ],
            "entityMap": {},
        }

        text = to_text(data)

        self.assertEqual(text, "Hallo  Hallo und so weiter")
