## [1.6.5](https://gitlab.com/openpatch/itembank-backend/compare/v1.6.4...v1.6.5) (2021-01-24)


### Bug Fixes

* filter unused edges ([5c81fba](https://gitlab.com/openpatch/itembank-backend/commit/5c81fba579306c09dc3e489b754fd6897fed6493))

## [1.6.4](https://gitlab.com/openpatch/itembank-backend/compare/v1.6.3...v1.6.4) (2020-06-22)


### Bug Fixes

* mocking of data ([12cbd4c](https://gitlab.com/openpatch/itembank-backend/commit/12cbd4cc5c05ef79736f70b1e97392c636442081))
